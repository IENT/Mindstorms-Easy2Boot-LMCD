# Easy2Boot LMCD

This software is a fork of [Easy2Boot](http://www.easy2boot.com/), modified to be used as ISO boot loader for the "[Lego Matlab Livecd](https://git.rwth-aachen.de/IENT-Internal/Praktikum-Mindstorms)" at IENT RWTH Aachen University.

Easy2Boot is a collection of scripts and programs on top of grub4dos (a fork of GNU grub). It allows the simple and automated creation of easily manageable, multi-bootable USB drives.

Easy2Boot offers several important features:
- can boot ISOs (grub4dos)
- can boot ISOs larger than 4GB from widely compatible NTFS partitions (grub4dos)
- extremely compatible (host BIOS and guest ISO)
- easy configuration

Easy2Boot contains a number of closed source binary executables for win32 which automate the creation of an Easy2Boot USB flash drive. In our use case it is only dependent on open source tools for execution. A USB drive can be created using only open source tools on a Linux system. Thus, the Easy2Boot distribution can be slimmed down accordingly.

## Usage

It takes 3 steps to set up this tool:

1. Create your E2B USB drive, considering the options explained in [the next section](#USB-drive-creation)
1. Add payload ISO: Usually the LMCD ISO which you created earlier. Apply necessary config for [autostarting](#Autostart).
1. Consider your options for [persistence](#Persistence) and set it up as necessary.

### USB drive creation

Easy2Boot drives can be created fully automatically on [Windows](#windows) using a graphical or command line interface or semi-automatically on [Linux](#linux).

During creation, you will have multiple options you should consider beforehand:
1. file system: although FAT32 and NTFS are both supported, we **MUST** choose NTFS for LMCDs as they are larger than the 4GB size limit imposed by FAT32.
1. CONTIG.ISO: payload ISOs must be contiguous (must not be fragmented) to be able to boot. If a payload ISO is not contiguous, Easy2Boot will try to use a "magic number" to boot, and if that fails will try to copy the ISO to CONTIG.ISO to makeit contiguous. In our case we most certainly are using USB drives with at least 16 GB capacity, only one LMCD payload ISO, and are not often updating the ISO. Since booting from CONTIG.ISO (copying) is rather slow, my advise is to refrain from using CONTIG.ISO. Should you wish to use it anyway, choose at least the size of your largest ISO and remember that CONTIG.ISO must be contiguous.
1. Persistence: While the LMCD ISO provides some options for persistence, it may not be good enough for your case. Easy2Boot offers a full system persistence mode: see [Persistence](#Persistence).

<!-- OPTIMIZE MFT
    NTFS reserves space within the file system for the Master File Table (MFT). This table stores all file, directory, and metafile data, including names, dates and permissions.
    By default, the reserved area starts 3 GB after the file system beginning. The Easy2Boot distro is not nearly 3 GB large, and neither are our payload ISOs small enough, thus this space is wasted. For optimization it might be possible to move the MFT by tricking NTFS.
    Note: mkntfs aka the linux script automatically places ntfs at the beginning.
    This was however not necessary at the point of writing and still remains to be investigated further.
    https://www.experts-exchange.com/articles/17308/A-trick-to-move-NTFS-System-Files-Also-known-as-Metadata-and-MFT-Zone-to-the-beginning-of-the-volume.html -->

#### Windows

1. `git clone` this repository or download and unpack the latest release.
1. Attach the USB drive to be formatted as Easy2Boot drive (all data on it will be destroyed!).
1. Execute `Make_E2B.exe` (GUI) or `MAKE_E2B_USB_DRIVE (run as admin).cmd` (CLI).
1. Choose NTFS as file system.
1. If you want to use CONTIG.ISO, make it at least as large as your largest ISO (e.g. 6 GB).

For more info, see http://www.easy2boot.com/make-an-easy2boot-usb-drive/

#### Linux

Requirements (install using `apt install` or similar):
- `bash`
- `coreutils`
- `parted`
- `ntfs-3g`
- `rsync`
- `sudo` or similar

1. Identify the device letter of your USB drive. It will look similar to `/dev/sdX` (no number, and X is a lowercase letter). These tools will list connected drives and display information: `lsblk` (block device tree), `lsusb` (connected USB devices), `fdisk -l /dev/sdX` (details about a block device). The most obvious property to look out for will be the drive size.
1. Unmount possibly mounted partions of the device with `sudo umount /dev/sdXY` where Y is a number.
1. Partition the USB drive using `sudo parted /dev/sdX`. Warning: All changes will be applied immediately!
    1. Type `print` to verify the status quo. If this is not the right disk, type `quit` to abort!
    1. Type `mklabel msdos` to create a new MBR partition table and confirm with `y`.
    1. Type `mkpart primary ntfs 1049kB -2049s` to create an aligned primary partition with NTFS type label and leaving some MiB aligned sectors free at the end. <!-- Type `toggle 1 boot` to set the bootable flag for the first partition. (should be done by the easy2boot script anyway) -->
    1. Type `mkpart primary ext2 -2048s -1986s` to create a 63 sectors large, second partition at the end of the drive.
    1. Verify the changes using `print`, then `quit`.
1. `git clone` this repository or download and unpack the latest release.
1. Open a bash(!) terminal and `cd` to `Mindstorms-Easy2Boot-LMCD/_ISO/docs/linux_utils`.
1. Make tools executable: `sudo chmod +x *`
1. Format the partition with NTFS file system using `sudo ./fmt_ntfs.sh`. Use the partition identifier (`/dev/sdX1`) of the partition you created earlier.

For more info, see http://www.easy2boot.com/make-an-easy2boot-usb-drive/make-using-linux/

#### NTFS Optimization

NTFS has some properties which are disadvantageous regarding the fact that we need our payload ISOs to be contiguous. These are the most common ones including methods to remedy them.
- The Master File Table is a hidden file that stores metadata about the files on the drive. Windows will usually place it around 3 GB into the file system, effectively wasting 3 GB of fragmented free space. Luckily the Linux version of the Easy2Boot creator will optimize this by placing the MFT at the beginning of the drive.
- The `$LogFile` is a similar case. It logs the operations in the file system.
- While Windows tends to write files to NTFS in clusters (all in one place) and usually starting from the beginning of the file system, this may not always be the case. Linux will often scatter files over the whole file system for other hardware optimization. Some defragmentation programs are able to remedy this issue at least in part (optimize or defragment free space).

All of these problems can also be solved by the following method which requires both a Linux and Windows system. Alternative tools may be available.
1. Check how much space is in use. Linux: `sudo ntfsresize -m /dev/sdX1`
1. Shrink the file system to the smallest possible size. Linux: `sudo ntfsresize -s NEWSIZE /dev/sdX1`, use the checked space as NEWSIZE. Note the original "Current volume size" for later.
1. Check the drive for errors. Windows: `chkdsk /f E:` where E: is your drive.
1. Expand the file system to the original size. Linux: `sudo ntfsresize -s ORIGSIZE /dev/sdX1`, where ORIGSIZE is the Current size from earlier.
1. Again check the drive for errors. Windows: `chkdsk /f E:` where E: is your drive.

#### Creating multiple Easy2Boot drives

Once you finish creating and optimizing your customized Easy2Boot-LMCD drive, it is advisable to save its current state by creating an image. This enables you to reset the stick to the current state, should anything go wrong, or replicate it with more USB drives. Note that you cannot clone an existing larger drive to a new smaller drive. If you clone a smaller drive to a larger drive, the excess storage space will not be available. You can however simply adjust the partitions (recreate with same start but later ending) and file system as described in the former sections.

To clone a drive, you could use programs like GNU dd (Windows and Linux), [Win32 Disk Imager](https://sourceforge.net/projects/win32diskimager/) (Windows), and many more.

Example of creating an image with dd:
```
sudo dd if=/dev/sdX of=/where/you/store/your/images.img bs=64M status=progress
```
Example of restoring an image with dd:
```
sudo dd if=/where/you/store/your/images.img of=sdX bs=64M status=progress
```

### Adding Payload ISOs

Due to how NTFS and Windows default copy work, it is almost guaranteed for a 5+ GB LMCD ISO to become fragmented when simply copying it to your Easy2Boot drive. For this reason it is advisable to use a tool which tries to avoid fragmentation by preallocating a large enough contiguous space on the drive before copying.
`rsync` is a free (GPLv3) tool included in many Linux distributions and also available for Windows (e.g. with [Git for Windows](https://git-scm.com/download/win)). However at the time of writing, rsync on Linux does not support `--preallocate` with ntfs-3g.

Usage example:
```
rsync -avh --preallocate /your/iso/folder/MindstormsEV3-2020.iso /your/e2b/drive/_ISO/MINDSTORMS/
```

For compatibility reasons, ISO names should not include spaces or special characters apart from `-` or `_`.

#### Autostart

The default configuration assumes the existence of an ISO which it will try to boot automatically. It can be configured in `/_ISO/MAINMENU/$$MindstormsDefault.mnu`.
If the configured ISO does not exist, then Easy2Boot will fall back to its main menu without password protection.

Also see [Booting](#booting).

#### Defragmenting

Should the ISO become fragmented anyway, you can try to defragment it using the included tools:
- Windows: execute `MAKE_THIS_DRIVE_CONTIGUOUS.cmd` in the drive root with Administrator privileges. The tool will simply close on success or display an error otherwise.
- Linux: cd to `_ISO/docs/linux_utils`, `sudo chmod 777 *`, `sudo ./udefrag -om /dev/sdX1`. See also: `_ISO/docs/linux_utils/ReadMe_fmt.sh.txt`

---

The modifications in this distro include isoboot magic for LMCD ISOs (http://www.easy2boot.com/faq-/isoboot/). This may allow Easy2Boot to boot a fragmented ISO in last instance.

### Persistence

Easy2Boot can boot in persistence mode, making it seem like the system is actually installed on the Easy2Boot drive. Any modifications will be saved and restored after a reboot. Persistence can be added by adding a simple menu item file on the Easy2Boot drive alongside the LMCD ISO.

The customized ISO for the "MATLAB meets LEGO Mindstorms" course has its own mechanism built in which can make the `/home/mindstorms/work` folder persistent in several ways. You can find information on the LMCD built-in persistence in the official [LMCD documentation](https://git.rwth-aachen.de/IENT-Internal/Praktikum-Mindstorms).

It may however be necessary or desirable to persist the full system, most importantly including the Matlab activation and student tasks which are usually downloaded automatically upon system boot, as well as any other modifications that may have been made. This allows the system to function even offline or at home if it has been booted at least once with access to the RWTH network before.

If you choose to use E2B's persistence, set it up as follows. The complete documentation of Easy2Boot persistence can be found on http://www.easy2boot.com/add-payload-files/persistence/.

#### Windows

1. Run `_ISO\docs\Make_Ext\Make_Ext.exe`
1. Use it to create a file `mindstorms-rw` in the root directory of your USB stick with ext4 (or ext3) partition scheme. Choose size according to space available on your stick, e.g. 4GB on a 16GB stick.
1. The "Volume name" **must** be `casper-rw`.
1. Make sure to [defragment](#Defragmenting) the `mindstorms-rw` file.
1. When booting, choose the "Persistent" menu entry. Set up [autostart](#Autostart) accordingly.

#### Linux

1. Mount and change directory to your usb stick in a terminal `cd /run/media/my/stick`.
1. Create an empty file: `sudo dd if=/dev/zero of=mindstorms-rw bs=64M count=64` for a good choice of 64M*64=4GB for a 16GB stick. Vary the count variable to change file size.
1. Create an ext4 filesystem on the partition file: `sudo mkfs.ext4 -F mindstorms-rw`.
1. Make sure the `mindstorms-rw` file is not [fragmented](#Defragmenting).
1. When booting, choose the "Persistent" menu entry. Set up [autostart](#Autostart) accordingly.

### Booting

1. Connect and select the USB drive as your boot device in BIOS.
1. Easy2Boot will search for bootable ISOs and then show an empty screen with a mindstorms wallpaper.
1. Upon timeout the default ISO [1] will be started.
1. Press ESC and enter the password (default: kuni45bert [2]) to interrupt automatic boot timeout.
1. Use the arrow keys to navigate the Easy2Boot menu and select an entry with Enter.
<!-- 1. Select a "persistent" entry to boot the ISO in persistent mode (changes will be kept after reboot). -->

[1]: The default ISO can be configured in `/_ISO/MAINMENU/$$MindstormsDefault.mnu`.

[2]: The password is set in `/_ISO/MyE2B.cfg` (`set pwd=kuni45bert`)

## Updating

1. `git checkout` the upstream branch which contains the original Easy2Boot code.
1. Delete all files in the working directory (except .git system files).
1. Download the latest Easy2Boot ZIP (usually labeled "For Linux users").
1. Unpack the ZIP archive to the working directory.
1. `git add -A` and `git commit` all changes. You might want to add a `git tag` of the version number.
1. `git checkout ` your dev branch. `git merge --no-commit` and review the changes, solve conflicts, `git rm` possible unnecessary files introduced by the update and finally commit.

## License

Easy2Boot-LMCD is for internal use at RWTH Aachen only, as it is not licensed for publication from the original author.

Based on Easy2Boot (www.easy2boot.com).
Easy2Boot is Freeware/Donationware.
See http://www.easy2boot.com/contact-us/licensing/ for more info.

See also `\_ISO\docs\Licenses`.

Contains WinContig.exe (https://wincontig.mdtzone.it/en/index.htm).
WinContig is freeware for personal and commercial use.

Contains RMPARTUSB.exe (www.rmprepusb.com).
RMPrepUSB is free for public, non-commercial use.

Contains LZMA.exe (public domain).
Contains getFileExtents.exe.

Contains GNU cpio.exe (GPLv3).

Contains grubinst.exe (GPLv2).

Contains GWT.exe (GetWaikTools).

Contains LockDismount.exe (and license attached in ReadMe).
LockDismount is free to use.

Contains GNU wget.exe (GPLv3).

Contains wtee.exe (https://github.com/rbuhl/wintee).
wintee is licensed under MOZILLA PUBLIC LICENSE Version 1.1.

Contains mke2fs.exe (https://sourceforge.net/projects/ext2fsd/).
It's a freeware, everyone can modify or distribute under GNU GPLv2.

Contains firadisk (ImDisk Virtual Disk Driver).
Free to use (see readme.txt).
