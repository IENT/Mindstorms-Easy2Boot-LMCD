ISO Name für Autostart: /your/e2b/drive/_ISO/MINDSTORMS/MindstormsEV3-2020.iso
Abweichender Autostart kann in /_ISO/MAINMENU/$$MindstormsDefaultISO.mnu bzw $$MindstormsDefaultPersist.mnu eingestellt werden.
Wenn Easy2Boot-Persistenz benutzt werden soll, lösche die Datei $$MindstormsDefaultISO.mnu.

Achtung! ISOs müssen zusammenhängend, dh nicht fragmentiert sein!
Nach Möglichkeit neue ISOs mit folgendem Befehl kopieren:
rsync -avh --preallocate /your/iso/folder/MindstormsEV3-2019-beta3.iso /your/e2b/drive/_ISO/MINDSTORMS/

Ggf ist es nötig nach dem kopieren den Stick zu defragmentieren.

Weitere Informationen: siehe README.md
https://git.rwth-aachen.de/IENT-Internal/Mindstorms-Easy2Boot-LMCD