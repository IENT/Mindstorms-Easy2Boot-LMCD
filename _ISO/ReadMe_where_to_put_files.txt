WHERE TO PUT YOUR PAYLOAD FILES
===============================

Copy your payload files (e.g. .ISO, .IMA, .VHD, .WIM, etc.) to one of the E2B Menu folders (\_ISO\XXXX).

For a simple test, just copy a linux LiveCD ISO file to \_ISO\MAINMENU folder and then run \MAKE_THIS_DRIVE_CONTIGUOUS.cmd.

Notes:  
	Windows Install ISOs   ->>  copy to correct \_ISO\WINDOWS\xxxx folder for best results.

	Remember to always run \MAKE_THIS_DRIVE_CONTIGUOUS.cmd after copying all your payload files.
	To start with, DO NOT USE SPACES IN FILE NAMES (most payloads will work if there are spaces in the filename, but some do not work).
	
Once you have a payload working, try renaming it so that it is more 'readable' in the menu or create a .txt file for it using \_ISO\TXT_Maker.exe or a simple text editor.

The menu system and most 32-bit ISOs can be tried using \QEMU_MENU_TEST (run as admin).cmd - but always test fully using a real system.

Check http://www.easy2boot.com/add-payload-files/list-of-tested-payload-files/ for a complete list of over 300 tested payload files and any special instructions.

1. Normal Menu folders
======================
Payload files (e.g. .iso, .ima, .VHD, etc.) can be put in any 2nd level (\_ISO\xxxx).
They will be automatically added to a menu.
E2B does not automatically search for payload files BELOW these folders, it only looks at the 2nd level folder.
E2B will search for .mnu (menu) files in the 2nd level folders AND ALL FOLDERS BELOW THESE FOLDERS.

\_ISO\MAINMENU            (files are listed in the MAIN Menu)
\_ISO\ANTIVIRUS           (files are listed in the ANTIVIRUS Sub-Menu)
\_ISO\BACKUP              (files are listed in the BACKUP Sub-Menu)
\_ISO\DOS                 (files are listed in the DOS Sub-Menu)
\_ISO\LINUX               (files are listed in the LINUX Sub-Menu)
\_ISO\UTILITIES           (files are listed in the UTILITIES Sub-Menu)
\_ISO\UTILITIES_MEMTEST   (files are listed in the MEMTEST Sub-Sub-Menu, under the UTILITIES Menu)
\_ISO\WIN                 (files are listed in the WINDOWS Sub-Menu, e.g. Windows To Go VHDs or .imgPTN files)
\_ISO\WINPE               (files are listed in the WINDOWS PE Sub-Menu)

These 'normal' menu folders all behave identically. For instance a linux ISO will work just as well in the \_ISO\WIN folder or the \_ISO\DOS folder.
The sub-menu entry will only appear in the Main Menu if there is at least one file or folder inside the sub-menu folder.
For instance, if you don't want a DOS Menu - simple delete the folder contents under \_ISO\DOS.

1.1 Main Menu folder
====================
\_ISO\MAINMENU is the folder that holds payload files that you want to be listed in the Main Menu.
\_ISO\MAINMENU\MNU can be used for special .mnu files + payload files. Payload files in this folder are not automatically added to the Main menu - so you must use a .mnu file.

1.2 Sub-Menu folders
====================
Pre-defined Sub-Menu folders are: ANTIVIRUS, BACKUP, DOS, BACKUP, DOS, LINUX, UTILITIES, WIN, WINPE
These behave in an identical way to the \_ISO\MAINMENU folder, except that they are sub-menus.
Sub-menu entries will only appear in the Main menu if the sub-menu folder contains one or more folders or files.

You can add your own sub-menu folders if you wish by using \_ISO\SUB_MENU_Maker.cmd. All sub-menu folders must be under \_ISO (2nd level only).

1.3 Sub-Sub-Menu folders
========================
\_ISO\UTILITIES_MEMTEST folder is a sub-menu of the UTILITIES sub-Menu.
This behaves in an identical way to the \_ISO\MAINMENU folder, except that it will appear in the MEMORY TEST sub-menu of the UTILITIES Menu.

1.4 'Special' Folders
=====================

\_ISO\AUTO (Direct Boot Menu) - Can be used for most payload files (all the payload files in all the sub-folders are listed in the menu).
				You can make as many sub-folders under \_ISO\AUTO as you like. They will all appear in the Direct Boot menu.
				This allows you to order the menu entries because file names and sub-sub-menu folder names are sorted alphabetically.
				Note: .mnu files are ignored - only payload files and .txt files wil be added to the menu.

\_ISO\WINDOWS\xxxxx\ - Only for Windows Install ".ISO" and ".imgPTN" files (files in the \_ISO\WINDOWS folder are ignored).

			IMPORTANT: For Windows Install ISO files, the E2B USB Flash drive works best on a REMOVABLE TYPE (not a hard disk or fixed disk).
			If your E2B USB drive is a hard disk, you may also need to connect a WinHelper USB Flash drive, 
                        or convert the Windows Install ISO to a .imgPTN file, otherwise you will may a 'A required CD/DVD drive device is missing' error.
			http://www.easy2boot.com/make-an-easy2boot-usb-drive/how-to-make-a-helper-usb-flash-drive/
				
			 Windows Installer ISOs must go in correct sub-folder or they will not work.
				e.g.	\_ISO\WINDOWS\WIN8\Win864-bit.iso
					\_ISO\WINDOWS\WIN10\Win10x86Pro.iso   
					\_ISO\WINDOWS\XP\XPProSP3.iso
					\_ISO\WINDOWS\WIN10\Win10Creatorx64English.imgPTN
				
			Tip: By using a .mnu file for each ISO, you can run any Windows Install ISO from any Menu.
			     Windows 7 may not detect a USB drive if it is connected to a USB 3 port (or a modern PC\Notebook).

               		Note: Windows XP Install .imgPTN files are NOT supported if in the \_ISO\WINDOWS\XP folder.

2. About .txt and .mnu files
============================

PAYLOAD FILES + .txt files  - .txt filename must match payload file - e.g. ubuntu.iso + ubuntu.txt
PAYLOAD FILES + .mnu files  - .mnu filename can be any name you like, usually the two files are placed in a \_ISO\xxxx\MNU folder.

2.1 Examples
============

\_ISO\ANTIVIRUS
 fred.iso   <- will be auto-detected
 fred.txt   <- will be used as the menu entry title for fred.iso if present (filename must be same)

\_ISO\ANTIVIRUS\MNU
 jim.iso    <- will NOT be auto-detected because not at top level of a menu folder (must use a .mnu file)
 jim.mnu    <- will be auto-detected and added to the menu (filename does not need to match the .iso)

.mnu files are detected at the 2nd level and all levels below.
Payload files (.iso, etc.) are only auto-detected at the 2nd level (i.e. top level of a menu folder).

NOTE: WINDOWS INSTALLER ISOs work best when placed in the correct sub-folder under \_ISO\WINDOWS.
      e.g. a Windows 7 Install ISO goes in \_ISO\WINDOWS\WIN7.


2.2 .mnu files
==============
.mnu files must start with 'title' or 'iftitle'.

If a .mnu file uses 'iftitle' then the test case in square brackets must be TRUE for the menu to appear, e.g.

  iftitle [if exist $HOME$/Ubuntu_32.iso] Ubuntu 32-bit Live CD \nBoot to Ubuntu

The menu entry 'Ubuntu 32-bit Live CD' will only be listed in the menu if Ubuntu_32.iso is in the same folder as the .mnu file.
.mnu files are detected at the 2nd folder level and all levels below that level.
$HOME$ when used in a .mnu file means 'current path of this .mnu file' - e.g. /_ISO/ANTIVIRUS.
$NAME$ when used in a .mnu file means 'filename, without extension, of the .mnu file' - e.g. Ubuntu_32
You can use $HOME$/$NAME$.iso as a 'portable' file specification. 
In this case, the .mnu filename must match the .iso filename.
Any text after \n in a 'title' line, defines the help text that appears under the menu.

For examples of .mnu files, see the \_ISO\docs\Sample mnu files folder.
Comment lines must begin with the # symbol.


2.3 .txt files
==============
.txt files can start with 'title' or 'iftitle'.

A .txt file should consist of a single line of text only, example:

  title Here is a menu entry\nHere is the help text below the menu\nHere is the 2nd line of help\nHere is the third line

The filename of the .txt file must exactly match the name of the payload file (e.g. ubuntu123.iso and ubuntu123.txt).
.txt files only work at the 2nd folder level in normal menu folders (\_ISO\xxxx) and in the \_ISO\WINDOWS\xxxx folders
Any characters after \n defines the content of the help text that appears under the menu (max 4 lines)

You can use the \_ISO\TXT_Maker.exe Windows GUI to create .txt files.


3. MORE INFORMATION
===================
If a .mnu file is placed in a valid menu folder at \_ISO\xxxx (2nd level) or anywhere below, it will be auto-detected by Easy2Boot.

WARNING: If a .mnu + .iso file are both placed in \_ISO\xxxx, you will get TWO MENU ENTRIES, one for the .mnu file and one for the .iso file.
For this reason, typically an ISO file and .mnu file are usually both placed at the 3rd level - e.g. \_ISO\LINUX\MNU\ or \_ISO\LINUX\ANY_NAME\.

Please visit www.easy2boot.com for more information.