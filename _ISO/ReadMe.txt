EASY2BOOT
=========

To make an E2B USB drive: Run \MAKE_E2B.exe as Administrator.

Also read \_ISO\docs\Make_E2B_USB_Drive\ReadMe.txt.

To add files
============

1. Copy bootable files (e.g. .ISO, .IMA, .IMG, .BIN, .VHD, .WIM), to the \_ISO\MAINMENU folder or one of the other menu folders, e.g. \_ISO\ANTIVIRUS.

   WINDOWS INSTALL ISOs should be copied to the correct sub-folder for best results, e.g. \_ISO\WINDOWS\WIN8\Windows8_64.iso.

2. Run \MAKE_THIS_DRIVE_CONTIGUOUS.cmd before booting - most payload files need to be contiguous (Windows ISO files may not need to be contiguous).

3. Boot to the E2B USB drive (MBR-only - you cannot UEFI-boot to the E2B menu).

      To UEFI-boot, you need to convert the ISO/payload to a .imgPTN file using the E2B MPI Tool Kit.
      You cannot UEFI-boot to the E2B menu system, you must first select a .imgPTN file by MBR-booting
      and using the E2B menu - OR - by running \_ISO\SWITCH_E2B.exe under Windows to select a .imgPTN file.
      Once the .imgPTN image has been 'switched-in' as a new partition, you can UEFI-boot from it (if it contains UEFI boot files) or MBR-boot from it.

Notes
=====

Always test using a real system NOT a Virtual Machine, because it may not always work on a VM.

Set the First Boot Device in the BIOS settings as the internal hard disk.
Use the BIOS Boot Selection pop-up menu to select the E2B USB drive and boot from it.

You can check that your E2B drive boots OK by running \QEMU_MENU_TEST (run as admin).cmd  (32-bit).
Some 32-bit payloads can even be run using QEMU too.

Avoid using file names with spaces or special characters such as & or ().
Once it is working OK, you can try using file name with spaces, or use a .txt file to change the menu entry text to whatever you like.

Do not use spaces in the file names of the following types of file: any linux ISO that uses persistence, any persistence file or any file booted via CONTIG.ISO.

If you get an 'Error 60: File for emulation must be in one contiguous area' message, you must make all the files contiguous. 
To make all files contiguous, just double-click on \MAKE_THIS_DRIVE_CONTIGUOUS.cmd to run WinContig.exe.

Please visit www.easy2boot.com for full instructions and watch the YouTube videos.

E2B lists the entries in alphanumeric order in the menu, depending on the file name and folder name.


UEFI
====
You cannot UEFI-boot directly from an ISO file using the E2B grub4dos menu system, you must convert each ISO to a .imgPTN file and then 'switch-in' the image.

To UEFI-boot, you need to convert each UEFI-bootable ISO to a .imgPTN file using the MPI Tool Kit.
Drag-and-drop the ISO onto the MPI_FAT32 Desktop shortcut to convert the ISO to a FAT32 partition image.
USB flash drives (Rufus, YUMI, WinSetupFromUSB, DOS, etc.) and other types of bootable files can also be converted to .imgPTN files. 
The ISO/payload must support UEFI-booting and so must already contain UEFI boot files in the \EFI\BOOT folder.

1. Copy the .imgPTN file to the E2B USB drive (e.g. \_ISO\MAINMENU).
2. Double-click on \MAKE_THIS_DRIVE_CONTIGUOUS.cmd to make the file contiguous.
3. Boot to E2B and select the .imgPTN file from the menu (or run \_ISO\SWITCH_E2B.exe and double-click on the .imgPTN file).
   Note: This will completely change the partitions on the USB drive. The E2B partition will no longer be acccessible.
4. Now the E2B USB drive will directly UEFI-boot (Secure Boot is possible if the UEFI boot files are signed correctly).

Tip: Use a file extension of .imgPTN23 if you want the 2nd partition to be accessible.

Read http://www.easy2boot.com/add-payload-files/adding-uefi-images/

Check http://www.easy2boot.com/add-payload-files/list-of-tested-payload-files/ for a list of supported bootable files.


E2B USB Hard Disks
==================
If your Easy2boot USB drive is of the 'Fixed disk' type (it appears as 'Local Disk' in Windows Explorer)
then you also may also need to connect a small USB HELPER Flash drive when booting Windows Vista/7/8/10 install ISOs.
This will only be needed if E2B cannot use 'WIMBOOT' by default.

See \_ISO\docs\USB FLASH DRIVE HELPER FILES for details.

If you prefer, you can convert the ISO to a .imgPTN file using MakePartImage (MPI Tool Kit), in which case a USB Helper Flash drive
is not needed and UEFI-booting may also work.

\_ISO\CONTIG.ISO (500MB) - Optional
===================================
This file is used by E2B ony if your ISO and other payload files are below 500MB and are not contiguous.
The file \_ISO\CONTIG.ISO needs to be contiguous.
E2B will copy a non-contiguous ISO file into CONTIG.ISO so that it will be contiguous and it will then boot from CONTIG.ISO instead of the original ISO file.

Tip: If you make sure all your payload files are contiguous (e.g. use  \MAKE_THIS_DRIVE_CONTIGUOUS.cmd or run RMPrepUSB - WinContig (Ctrl+F2)) 
then you can delete CONTIG.ISO to save 500MB of space.

\_ISO\SWITCH_E2B
================
This is a Windows 32-bit utility which allows you to switch-in or switch-out .imgPTN image partition files without needing to MBR-boot to the E2B menu system..

See http://www.easy2boot.com/add-payload-files/makepartimage/ and http://www.easy2boot.com/add-payload-files/switch-e2b/
for more information.

\_ISO\E2B_Editor.exe
====================
This is a Windows 32-bit utility which allows you to design your own E2B menu and set some configuration options.

Load \_ISO\MyE2B.cfg (or \_ISO\Sample_MyE2B.cfg) and \_ISO\Sample_MyBackround.jpg (or your own .bmp or .jpg file).

You can change the wallpaper, language, main heading, text colours, menu options, etc. 

Note: E2B always uses \_ISO\MyE2B.cfg as the user config file.

See http://www.easy2boot.com/configuring-e2b/e2b-editor/ for more details.

Do NOT edit any of the E2B files except \_ISO\MyE2B.cfg (otherwise you will lose your changes if you update E2B to a later version).

See http://www.easy2boot.com/configuring-e2b/changing-the-e2b-preset-text/ for details on how make any changes.

\_ISO\TXT_Maker.exe
===================
Each of your added payload file can have a .txt file of the same file name which specifies the menu entry text (and hotkey) for it.
The text in the .txt file will be used for the menu entry, instead of the name of the actual payload file.

 e.g.    title Here is the new menu entry\nHere is the help text below the menu\nHere is the 2nd help line

The TXT_Maker.exe Windows GUI utility allows you to create .txt files for your payload files.
Just drag-and-drop a payload file onto the filename area and then fill in the Menu entry and Help lines.

\_ISO\SUB_MENU_Maker.cmd
========================
Use this batch file to make a new menu folder and new Main menu entry.
You can choose the menu heading, menu entry text, hotkey (optional) and menu entry help text.

\_ISO\PimpMyDrive.cmd
=====================
This adds some extra useful (cool!) .mnu files to E2B's Main Menu.

WARNING: To use the Change Theme menu, the user's \_ISO\MyE2B.cfg file must be overwritten and a backup will be made for you.

The new .mnu files are added to the \_ISO\MAINMENU\PIMP folder. You can delete or move these as required or just delete the whole PIMP folder.

See http://www.easy2boot.com/configuring-e2b/pimpmydrive/


Licensing
=========
The Easy2Boot Menu system is comprised of mainly unaltered GPL licensed s/w binaries such as grub4dos, memdisk, iPXE wimboot, etc.
The 'source code' for the E2B menu system is the plain-text grub4dos batch files and menu files and so is compliant with GPL licensing.
The E2B project download also includes other command-line GPL\Apache-licensed software which are bundled in for user convenience. 
These 3rd-party utilities are called from various other batch files which can be used to prepare\modify\repair your E2B drive.
They are not used by the E2B menu system itself.
You can see the licence terms for each of the unaltered binaries at \_ISO\docs\Licenses and http://www.easy2boot.com/contact-us/licensing/.
The E2B project download includes some compiled binary Windows executables written by me (SSi) - these are closed source but they do not contain GPL or other licensed code.

Want More?
==========
See http://rmprepusb.blogspot.com/p/easy2boot-useful-blogs.html for more ideas.

Please visit www.easy2boot.com for full details.
