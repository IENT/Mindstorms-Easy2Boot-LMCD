\_ISO\docs files and folders
============================

	Boot_me_using_QEMU - files used to run the QEMU emulator
        ChocBox       - These files are usd to create offline install packages for use with Chocolatey
	Download URLs - useful links to web pages
	E2B Utilities - various useful utilities for E2B. Read the ReadMe.txt file for details.
	FASTLOAD      - contains the FASTLOAD.YES file needed to enable Fastload on E2B (read the ReadMe.txt file)
	Fonts         - contains a selection of English fonts which can be used for E2B menus
	GFXBoot       - double-click on repack.cmd to make a new message file.
	linux_utils   - contains a linux .sh script which will make an E2B USB drive for you
	Make_E2B_USB_Drive - contains a Windows .cmd script which will make a E2B USB drive for you
	Make_Ext      - Make an ext2/3/4 file for use as linux persistence files
        MyThemes      - contains files and instructions which allow you to test a variety of different templates\themes\skins
	PassPass      - allows you to bypass Windows login password om local accounts
	Sample mnu files - useful grub4dos menu files (.mnu) which can be added to your E2B menus
        SysInfo       - Portable .hta file - run under Windows to list hardware and software
	Templates     - a variety of 'themes' or skins\template files - read the ReadMe.txt file for more information
        Tunes         - various grub4dos batch files which play a tune through the internal PC Speaker on the mainboard
	USB FLASH DRIVE HELPER FILES - contains files needed to make the E2B HELPER USB Flash drive
        UtilMan       - cmd scripts used to automate the UtilMan Windows hack (full admin access to Windows)
	Wincontig     - contains WinContig.exe to make files contiguous

	E2B_DPMS2_ReadMe.txt - Instructions on how to add 32-bit XP Mass Storage drivers to your E2B drive (may not be present on DPMS version)
	E2B_ReadMe.html      - html help
	PatchMe              - this is a grub4dos program which modifies grub4dos on your E2B USB drive (MBR+grldr)
	PatchMe_ReadMe.txt   - Instructions on how to run PatchMe
	PatchMyMBR           - patches the code in the grub4dos MBR only to make it boot silently

Please visit www.easy2boot.com for more details.
