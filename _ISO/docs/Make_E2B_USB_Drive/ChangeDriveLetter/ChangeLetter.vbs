' Change the Drive Letter of a removable volume with the next available letter
' Run as Admin

' Specify drive letter as a parameter
If WScript.Arguments.Count = 0 Then
	strDrive = InputBox( "Enter drive letter to change (e.g. F) : ",  "Choose drive")
Else
 strDrive = WScript.Arguments.Item(0)
End If

strComputer = "."

Set objWMIService = GetObject("winmgmts:" & "{impersonationLevel=impersonate}!\\" & strComputer & "\root\cimv2")

' Drive type - 2:removable, 3:local, 4:network
Set colVolumes = objWMIService.ExecQuery ("Select * from Win32_Volume Where Name = '" & strDrive & ":\\' And DriveType = 2")
'Set colVolumes = objWMIService.ExecQuery ("Select * from Win32_Volume Where Name = '" & strDrive & ":\\'")

set objFS=CreateObject ("Scripting.FileSystemObject")
For Each objVolume in colVolumes

 'Find out next available drive letter
 set colDrives=objFS.Drives
 letter = Asc(Ucase(strDrive)) + 1
 
'list free letters
 For i = Asc("D") to Asc("Z")
 If objFS.DriveExists(Chr(i) & ":") Then
	Else
	dlist = dlist & UCASE(chr(i)) & " "
 End If
 Next

strAnswer = InputBox(dlist & vbcrlf & "Enter new letter (e.g. F) : ",  "New letter")
	   objVolume.DriveLetter = UCASE(strAnswer) & ":"
	   objVolume.Put_
	   Wscript.Quit


Wscript.Echo "There are no available drive letters after " & strDrive & " on this computer."
 
Next
