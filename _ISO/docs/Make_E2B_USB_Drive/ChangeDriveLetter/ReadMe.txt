ChangeLetter.cmd
================

Allows you to change the currently assigned drive letter of a Removable drive.

Enter just the letter of the target volume - e.g. F    (do not use F: or F:\)
Enter new letter that you want - e.g. U                (do not use U: or U:\)

Only works if you specify the letter of a Removable drive.

Do not run the .vbs file directly as it requires Administrator privileges.