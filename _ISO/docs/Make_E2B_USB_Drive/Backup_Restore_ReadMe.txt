
Backup_E2B_drive_to_MASTER.cmd    - creates an E2B_MASTER folder on your Desktop containing the contents of the E2B drive

Restore_E2B_drive_from_MASTER.cmd - makes the contents of the E2B drive the same as the contents of E2B_MASTER

Robocopy must be pre-installed (if using XP, please install Robocopy).

These .cmd files can be used to duplicate E2B drives.
Only files and folders are copied (it does not make a drive bootable).
After completion, the contents of the E2B_MASTER folder will be the same as the contents of the E2B drive.
Any extra files will be removed.
Only the main E2B drive volume is duplicated. If your E2B drive has two partitions, the second partition is not copied.

After files have been restored, WinContig will be run to ensure that the new files are contiguous.

You can run the .cmd files directly from the USB drive that you want to duplicate, or you can copy the .cmd files to your Desktop and run them from the Desktop (you will be prompted for the E2B drive letter).

If you have more than one type of E2B USB drive, you can rename the .cmd files and edit the 'set MASTER_NAME=' line so that you can have a different backup folder for each E2B drive - e.g. E2B_MASTER_WIN, E2B_MASTER_AV, E2B_MASTER_MEDICAT, etc.
