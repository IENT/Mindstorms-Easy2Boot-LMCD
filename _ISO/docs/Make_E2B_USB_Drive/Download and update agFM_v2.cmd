@echo off
if NOT "%1"=="RUN" (
copy "%~0" temp_update.cmd
temp_update.cmd RUN
)


cls
TITLE %~n0 v1.01
:: set the cmd shell window size
mode con: cols=130 lines=56 2> nul
color 1f

REM Find agFM mounted drive
set USBDRIVE=
set exit=
FOR %%D IN (A B C D E F G H I J K L M N O P Q R S T U V W X Y Z) DO (
DIR %%D:\boot\grubfm\config > nul 2>&1 && DIR %%D:\grubfmx64.efi > nul 2>&1 && (call set USBDRIVE=%%D:)
)
set PTN2=%USBDRIVE%
echo.
echo.
echo =================== UPDATE UEFI BOOT FILES =====================
echo.
if "%PTN2%"=="" echo ERROR: Cannot find agFM files on 2nd partition (may need Windows 10?) & pause & goto :eof
::call :getdate "%PTN2%\grubfmx64.efi"
::set efidate=%fdate%
:: echo Current %PTN2%\grubfmx64.efi is dated: %efidate%

if exist %PTN2%\agFM_version.txt echo | set /p ask=Current version is & type %PTN2%\agFM_version.txt
echo.
echo.
echo Downloading current agFM zip file - please wait...
:: agFM_download_files.zip - overwrite file if already exists
wget.exe -q -t 1 -T 5 http://files.easy2boot.com/200003811-7d94f7d951/agFM_download_files.zip -OagFM_download_files.zip
if not exist agFM_download_files.zip echo WARNING: Failed to download agFM files from E2B website. & pause & goto :eof
echo.
call :getdate agFM_download_files.zip
echo agFM download zip file   is dated: %fdate%
echo.
set ask=N
set /p ask=Add a1ive grub2 File Manager UEFI boot files to %PTN2% (Y/[N]) : 
if /i not "%ask:~0,1%"=="Y" goto :JJ1

echo Extracting a1ive grub2 UEFI boot files to %PTN2%...
7z.exe  x -y agFM_download_files.zip -o%PTN2%\  -xr!7z.*
::if exist agFM_download_files.zip del agFM_download_files.zip
echo.
if exist %PTN2%\agFM_version.txt echo | set /p ask=Version is now & type %PTN2%\agFM_version.txt
echo.
if exist "%PTN2%\e2b\Update agFM\Download and update agFM.cmd" del "%PTN2%\e2b\Update agFM\Download and update agFM.cmd"

:JJ1
echo.
color 2f
echo FINISHED
pause
del temp_update.cmd & goto :EOF

:getdate
set fdate=%~t1
goto :EOF




