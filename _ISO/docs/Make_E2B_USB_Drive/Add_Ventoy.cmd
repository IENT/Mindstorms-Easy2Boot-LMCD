@echo off

cls
echo.
echo **********************************
echo *                                *
echo *   Add Ventoy to Easy2Boot      *
echo *                                *
echo * Usage:                         *
echo * Drag-and-drop Ventoy .zip file *
echo * onto this file.                *
echo *                                *
echo **********************************
echo.
echo Extracts Ventoy files and adds them to Partiton 2 of the E2B drive
echo.
echo Ventoy .zip file should be downloaded from https://github.com/ventoy/Ventoy/releases
echo.
echo Looking for \e2b\grubfm.iso on Partition 2...
FOR %%D IN (A B C D E F G H I J K L M N O P Q R S T U V W X Y Z) DO (
DIR %%D:\e2b\grubfm.iso > nul 2>&1 && (call set PTN2=%%D:)
)

if "%PTN2%"=="" echo WARNING: Cannot find Partition 2 of E2B drive. & echo.
if not "%PTN2%"=="" echo FOUND E2B PARTITION 2 at %PTN2%

REM if not "%~p0"=="\_ISO\docs\Make_E2B_USB_Drive\" echo ERROR: Must use file \_ISO\docs\Make_E2B_USB_Drive\%~nx0 on E2B USB drive &  pause & goto :EOF
set SRC=%1
if "%SRC%"=="" start https://github.com/ventoy/Ventoy/releases & echo ERROR: No Ventoy .zip file specified & echo. & echo Please drag-and-drop Ventoy .zip file onto this file & pause & goto :EOF
if not exist %SRC% echo ERROR: No Ventoy .zip file specified & echo Drag-and-drop Ventoy .zip file onto this file & pause & goto :EOF
if not "%~x1"==".zip" echo ERROR: Must use Ventoy .zip file as source file & pause & goto :EOF

REM Find temp folder
set vtmp=%~dp0\ventoy
if exist "%TMP%" set vtmp=%TMP%\ventoy
if exist "%TEMP%" set vtmp=%TEMP%\ventoy

echo Using temporary folder at %vtmp%

pushd "%~dp0"
if not exist 7z.exe echo ERROR: 7z.exe not present in %~dp0 & pause & goto :EOF
if not exist wget.exe echo ERROR: wget.exe not present in %~dp0 & pause & goto :EOF

REM delete temp folders
if exist "%vtmp%" rd /s /q "%vtmp%"
if exist "%vtmp%\ventoy.disk.img*" del "%vtmp%\ventoy.disk.img*"

7z e %SRC% v*.xz -o"%vtmp%" -r > nul
7z e "%vtmp%\ventoy.disk.img.xz" v*.img -o"%vtmp%" -r  > nul
7z x "%vtmp%\ventoy.disk.img" -o"%vtmp%"  -r  > nul
if not exist "%vtmp%\efi\boot\grubx64_real.efi" goto :EXT_ERR

if exist "%vtmp%\ventoy.disk.img.xz" del "%vtmp%\ventoy.disk.img.xz"
if exist "%vtmp%\ventoy.disk.img" del "%vtmp%\ventoy.disk.img"

if exist "%vtmp%\efi\boot\bootx64.efi" del "%vtmp%\efi\boot\bootx64.efi"
ren "%vtmp%\efi\boot\grubx64_real.efi" ventoyx64.tmp
if exist "%vtmp%\efi\boot\*.efi" del "%vtmp%\efi\boot\*.efi"
ren "%vtmp%\efi\boot\ventoyx64.tmp" ventoyx64.efi
if exist "%vtmp%\*.cer" del "%vtmp%\*.cer"

echo.
echo Downloading core.img from github...
md "%vtmp%\e2b"
wget -q -t 1 -T 5 https://github.com/ventoy/Ventoy/blob/master/INSTALL/grub/i386-pc/core.img -O "%vtmp%\e2b\core.img"

echo.
dir /b "%vtmp%\"
dir /b "%vtmp%\efi\boot\"
dir /b "%vtmp%\e2b\"
echo.

REM Check
if not exist "%vtmp%\grub\grub.cfg" echo ERROR: \grub\grub.cfg missing & goto :EXT_ERR
if not exist "%vtmp%\tool\*" echo ERROR: \tool files missing & goto :EXT_ERR
if not exist "%vtmp%\ventoy\ventoy.cpio" echo ERROR: \ventoy\ventoy.cpio missing & goto :EXT_ERR
if not exist "%vtmp%\e2b\core.img" echo ERROR: \e2b\core.img missing & goto :EXT_ERR
if not exist "%vtmp%\efi\boot\ventoyx64.efi" echo ERROR: \EFI\BOOT\ventoyx64.efi missing & goto :EXT_ERR

echo All folders need to be copied to Partition 2 of the E2B drive...
if "%PTN2%"=="" echo WARNING: Cannot find Partition 2 of E2B drive - please use Windows 10 & echo Press a key to view files & pause
if "%PTN2%"=="" start explorer.exe "%vtmp%"
if "%PTN2%"=="" echo Please copy files to Partition 2 of your E2B USB drive
if "%PTN2%"=="" pause & goto :EOF

REM copy folder
echo.
set ask=
set /p ask=OK to copy Ventoy files to %PTN2%\ ? (Y/N) : 
if /i not "%ask%"=="Y" rd /s /q "%vtmp%" & goto :EOF
xcopy /herky "%vtmp%" %PTN2%\
if not errorlevel 1 color 2f & echo Files copied OK


REM tidy up
rd /s /q "%vtmp%"
pause
goto :EOF



:EXT_ERR
color 4f
echo ERROR EXTRACTING FILES
pause



