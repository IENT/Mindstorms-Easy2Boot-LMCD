@echo off
REM Change the next line if you want a different name for the Master Backup folder
set MASTER_NAME=E2B_MASTER
set SRC=%USERPROFILE%\Desktop\%MASTER_NAME%

color 1f
title Restore E2B drive
set USB=%~d1
if "%USB%"=="" set USB=%~d0
echo.
echo      ******************************************
echo      *                                        *
echo      *         RESTORE TO E2B DRIVE           * 
echo      *                                        *
echo      ******************************************
echo.
echo.
if not exist "%SRC%" echo ERROR: %SRC% folder does not exist & color 4f & pause & goto :EOF
if not exist "%SRC%\_ISO\e2b\grub\e2b_grub.txt" echo ERROR: Backup folder does not contain E2B files! & color 4f & pause & goto :EOF

if not exist %USB%\_ISO\e2b\grub\e2b_grub.txt set /p USB=Please enter E2B USB drive letter : 
cls
set USB=%USB:~0,1%
set USB=%USB%:
echo.
echo      ******************************************
echo      *                                        *
echo      *             RESTORE TO %USB%              * 
echo      *                                        *
echo      ******************************************
echo.
echo.

if /i "%USB%"=="C:\" echo ERROR: Cannot copy to drive C: & color 4f & pause & goto :EOF
if not exist %USB%\_ISO\e2b\grub\e2b_grub.txt echo ERROR: %USB% is not an E2B drive! & color 4f & pause & goto :EOF

title Restore E2B drive      %SRC%  -----^>  %USB%
echo.
echo      %SRC%  -----^>  %USB%
echo.
set MT=
robocopy /? | %WINDIR%\System32\find.exe "MT[" > nul && set MT=/MT:1
echo.
echo WARNING: All files on %USB% will be synchronised with the master copy in %MASTER_NAME%
echo.
echo and any files on %USB% which are not in the master copy will be deleted.
echo.
echo.
echo.
echo Update all files on %USB% ? & echo. & pause
cls
echo     Copy %SRC%  -----^>  %USB%
echo.
REM Replace /MIR with /E if you only want changed and extra files to be added (does not delete files from target)
robocopy "%SRC%" %USB%\  /MIR /FFT /NJH /NJS /ETA /W:3 /R:3 %MT%
if errorlevel 8 color 4f & echo ERROR: There was a problem with Robocopy! & pause & goto :EOF

set NC=%errorlevel%
echo.
if "%NC%"=="0" echo Finished. & echo. & pause & goto :EOF
REM \System Volume Information is not copied - so usual errorlevel is 2
if "%NC%"=="2" echo Finished. & echo. & pause & goto :EOF
REM echo ROBOCOPY ERRORLEVEL=%NC%
echo.
echo Finished - press ENTER to make any new files contiguous...
pause > nul
if exist %USB%\_ISO\docs\WINCONTIG\MAKE_THIS_DRIVE_CONTIGUOUS.cmd call %USB%\_ISO\docs\WINCONTIG\MAKE_THIS_DRIVE_CONTIGUOUS.cmd
echo.

