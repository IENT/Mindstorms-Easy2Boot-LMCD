@echo off
pushd "%~dp0"
:skipa
echo CHOOSE DRIVE AND PARTITION 1 SIZE
echo =================================
echo.
echo PARTITION 2 will be expanded to fit remaining space
echo.
REM list all USB drives so user can see them and their drive number
RMPARTUSB LIST  %ALL% | %WINDIR%\System32\find.exe "DRIVE"
echo.
REM Ask user for a drive number
set DD=
set /P DD=TARGET USB DRIVE NUMBER (e.g. 2) : 
if "%DD%"=="" echo. & echo Please enter a number! & pause & cls & goto :skipa
set /A DD=%DD%+0
if "%DD%"=="0" echo. & echo Please enter a number above 0! & pause & cls & goto :skipa
echo.
echo PARTITION 2 will be expanded to fit remaining space
echo.
set /P P1=CHOOSE SIZE FOR PARTITION 1 in GiB (e.g. 50) : 
set /A P1=%P1%+0
if "%P1%"=="0" echo Please enter a number above 0! & pause & cls & goto :skipa
::echo DRIVE=%DD% P1=%P1%000
echo.
echo DRIVE %DD%
echo PARTITION 1 will be NTFS %P1%000 MiB
echo PARTITION 2 will be FAT32 (if <32GiB) or NTFS (if >32GiB)
echo.
REM ensure we expand 2nd partition...
set EXPAND2=EXPAND2
REM if "%1"=="AUTO" (set AUTODRV=%2) & (set AUTOLANG=%3) & (set AUTOKBD=%4) & (set AUTOSIZE=%5)
call Make_E2B_USB_Drive.cmd AUTO %DD% "" "" %P1%000
