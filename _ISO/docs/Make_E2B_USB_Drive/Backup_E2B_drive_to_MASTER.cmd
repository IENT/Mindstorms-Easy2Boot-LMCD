@echo off
REM Change the next line if you want a different name for the Master Backup folder
set MASTER_NAME=E2B_MASTER
set DEST=%USERPROFILE%\Desktop\%MASTER_NAME%

color 1f
title Backup E2B drive to %DEST%
set USB=%~d1
if "%USB%"=="" set USB=%~d0
echo.
echo      ******************************************
echo      *                                        *
echo      *       MAKE A BACKUP OF E2B DRIVE       *
echo      *                                        *
echo      ******************************************
echo.
echo.
if not exist %USB%\_ISO\e2b\grub\e2b_grub.txt set /p USB=Please enter E2B USB drive letter : 
set USB=%USB:~0,1%
set USB=%USB%:
cls
echo.
echo      ******************************************
echo      *                                        *
echo      *          MAKE A BACKUP OF %USB%           *
echo      *                                        *
echo      ******************************************
echo.
echo.

if /i "%USB%"=="C:" echo ERROR: Cannot backup drive C: & color 4f & pause & goto :EOF
if not exist %USB%\_ISO\e2b\grub\e2b_grub.txt echo ERROR: %USB% is not an E2B drive! & color 4f & pause & goto :EOF
REM Check if old copy of robocopy - use single thread with USB drives if possible for faster copying
set MT=
robocopy /? | %WINDIR%\System32\find.exe "MT[" > nul && set MT=/MT:1
title Backup E2B drive          %USB%  -----^>  %DEST%
echo.
echo      %USB%  -----^>  %DEST%
echo.
echo.
echo WARNING: All files on %USB% will be synchronised (mirrored)
echo with the master copy in the %MASTER_NAME% folder on your Desktop.
echo.
echo Files in the %MASTER_NAME% folder which are not on %USB% will be deleted.
echo.
echo.
echo.
echo OK to make the %MASTER_NAME% backup the same as %USB% ? & echo. & pause
echo.

cls
if not exist "%DEST%\nul" mkdir "%DEST%" 2> nul && echo %DEST% folder created.
if not exist "%DEST%" echo ERROR: Cannot make %DEST% folder! & color 4f & pause & goto :EOF
echo.
echo     Backup %USB%  -----^>  %DEST%
echo.
REM Replace /MIR with /E if you only want changed and extra files to be added (does not delete files from target)
robocopy %USB%\ "%DEST%" /MIR /FFT /NJH /ETA /NJS /W:3 /R:3 %MT%  /XF "pagefile.sys" "hiberfil.sys" /XD "$RECYCLE.BIN" "RECYCLER" "System Volume Information" 
REM fix bug in Robocopy which makes folder hidden and system!
attrib  -h -s -r "%DEST%" /d > nul
echo.
echo Finished.
echo.
pause
