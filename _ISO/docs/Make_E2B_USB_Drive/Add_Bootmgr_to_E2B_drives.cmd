@echo off
pushd %~dp0"
cls
color 1f
echo.
echo ***********************************
echo *                                 *
echo *     Add_Bootmgr_to_E2B          *
echo *                                 *
echo ***********************************
echo.
echo This will download a Windows 8.1 version of bootmgr and add it to any E2B drive in your system.
echo.
echo Connect your E2B USB drive(s) now and press ENTER when ready...
pause > nul
echo.
GWT -range:8808448-9236127 -out:"bootmgr" -title:"Download Win8.1 Bootmgr" -link:/download/B/9/9/B999286E-0A47-406D-8B3D-5B5AD7373A4A/9600.16384.WINBLUE_RTM.130821-1623_X86FRE_ENTERPRISE_EVAL_EN-US-IRM_CENA_X86FREE_EN-US_DV5.ISO -silent

if not exist bootmgr (
	color 4f
	echo ERROR: bootmgr was not downloaded!
	echo An Internet connection is required for curl.
	pause
	goto :EOF
)
echo Downloaded %~dp0bootmgr OK
echo.
set USBDRIVE=
echo Copying bootmgr to all E2B drive(s)...
echo.
REM omit A, B and C (floppy drive can give exception error STATUS_NO_MEDIA_IN_DEVICE 0xC0000013 if exists and empty in early Windows OS XP,2003,Vista)
FOR %%D IN (D E F G H I J K L M N O P Q R S T U V W X Y Z) DO (
if exist %%D:\_ISO\E2B\grub\e2b.cfg copy /Y bootmgr %%D:\_ISO\e2b\grub\DPMS\NTBOOT.MOD > nul && if exist %%D:\_ISO\e2b\grub\DPMS\NTBOOT.MOD\bootmgr echo E2B Drive %%D: updated && set USBDRIVE=%%D
)
echo.
if "%USBDRIVE%"=="" echo ERROR: NO E2B DRIVES FOUND (D: to Z:)! & echo. & color 4f
if not "%USBDRIVE%"=="" color 2f
pause

