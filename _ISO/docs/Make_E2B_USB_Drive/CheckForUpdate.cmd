@if not "%DEBUG%"=="1" @echo off
cls
TITLE %~n0 v1.3
:: set cmd shell window size
mode con: cols=80 lines=60 2> nul
color 1f
echo Check for latest E2B version
echo ============================
echo.
set SRCDL=%~d0
pushd "%~dp0"

:: Check for more recent version
if exist LatestE2B.txt del LatestE2B.txt
echo.
::wget.exe -q -t 1 -T 5 http://files.easy2boot.com/200001618-08ab00a9dd/LatestE2B.txt -O LatestE2B.txt > nul
echo Getting file LatestE2BBeta.txt from easy2boot.com - please wait...
if exist wget.exe set WGET=wget.exe
if exist "%~d0\_ISO\docs\Make_E2B_USB_Drive\wget.exe" set WGET="%~d0\_ISO\docs\Make_E2B_USB_Drive\wget.exe"
if "%WGET%"=="" echo INFO: Cannot find %~d0\_ISO\docs\Make_E2B_USB_Drive\wget.exe & goto :bits
%WGET% -q -t 1 -T 5 http://files.easy2boot.com/200002478-b51cbb6169/LatestE2BBeta.txt -O LatestE2B.txt > nul
:bits
if not exist LatestE2B.txt bitsadmin.exe /transfer 'e' http://files.easy2boot.com/200002478-b51cbb6169/LatestE2BBeta.txt "%~dp0\LatestE2B.txt" > nul
if not exist LatestE2B.txt echo ERROR: Cannot access internet? && pause && exit
%WINDIR%\System32\find.exe LatestE2B.txt "set LVER=" > nul || echo ERROR: Bad file downloaded? && pause && exit
set LVER=
set VER=
set LDATE=
set BVER=
set BDATE=
setlocal enableextensions enabledelayedexpansion
FOR /F "tokens=2 delims=^=" %%K IN (latestE2B.txt) DO (
    if "!LVER!"=="" (SET LVER=%%K) else if "!LDATE!"=="" SET LDATE=%%K
)
FOR /F "tokens=2 skip=2 delims=^=" %%K IN (latestE2B.txt) DO (
    if "!BVER!"=="" (SET BVER=%%K) else if "!BDATE!"=="" SET BDATE=%%K
)

if exist LatestE2B.txt del LatestE2B.txt

:: get current version of E2B
if exist "%temp%\mdtemp.cmd" del "%temp%\mdtemp.cmd" > nul
if exist "%~dp0..\..\e2b\grub\E2B.cfg" set CFG="%~dp0..\..\e2b\grub\E2B.cfg"
if exist "%~d0\_ISO\e2b\grub\E2B.cfg" set CFG="%~d0\_ISO\e2b\grub\E2B.cfg"
if "%CFG%"=="" echo ERROR: Cannot find \_ISO\e2b\grub\E2B.cfg & pause & goto :EOF
%WINDIR%\System32\find.exe "set VER=" %CFG% > "%temp%\mdtemp.cmd"
call "%temp%\mdtemp.cmd" > nul 2> nul
if exist "%temp%\mdtemp.cmd" del "%temp%\mdtemp.cmd" > nul
echo.
set VER=X%VER%
if /i "%VER:~1,1%"=="v" set VER=%VER:~2,99%
if "%VER%"=="X" set VER=
if not "%VER%"==""  echo INFORMATION: Your E2B version = %VER%
if not "%LVER%"=="" echo              Current release  = %LVER%   %LDATE%
if not "%BVER%"=="" echo              Latest Beta      = %BVER%  %BDATE%
echo.

if "%VER%"=="%LVER%" (
	echo You have the latest stable version
	color 2f
	echo.
)

if not "%VER%"=="%LVER%" (
	echo You do not have the latest stable version. 
	color 4f
	echo.
)

:PP
set "URL1=http://www.easy2boot.com/download/"
set "URL2=https://app.box.com/s/oqxlxqs6skcutjrfce8kwaudyttn16qc"
:: must escape funny characters with hat symbol
set "URL3=https://1drv.ms/f/s^!AqlrQcdsFA-KeES7GUgcjXrRvWY"

echo.
echo DOWNLOAD OPTIONS
echo ================
echo.
echo 1. Easy2Boot Download page          (stable version)
echo 2. Box Alternate Download page      (stable+Beta versions)
echo 3. OneDrive Alternate Download page (stable+Beta versions)
echo 4. Exit
echo.
choice /C 1234 /M "Choose a number: "
if errorlevel 4 exit
if errorlevel 3 start "" "!URL3!" && exit (0)
if errorlevel 2 start "" "!URL2!" && exit (0)
if errorlevel 1 start "" "!URL1!" && exit (0)

::goto :PP
exit (0)

