@echo off
echo.
echo Remove 'color nn' from all .cmd files
echo =====================================
echo.
echo Usage: Drag-and-drop a folder containing .cmd files onto this file
echo to remove all color commands from all .cmd files.
echo.
echo Copy fnr.exe (http://findandreplace.io/) to C:\Windows\System32 or this folder first
echo.
if not "%1"=="" echo About to remove all color nn commands from folder %1
echo.
pause
if "%1"=="" goto :eof
echo.
fnr.exe --cl --dir "%1" --fileMask "*.cmd" --includeSubDirectories --useRegEx --skipBinaryFileDetection --showEncoding --find "color [0-9a-fA-F][0-9a-fA-f]" --replace "color"
if %errorlevel% EQU 9009 echo. && ERROR: Please copy fnr.exe (http://findandreplace.io/) to C:\Windows\System32 or this folder first.
echo.
pause
