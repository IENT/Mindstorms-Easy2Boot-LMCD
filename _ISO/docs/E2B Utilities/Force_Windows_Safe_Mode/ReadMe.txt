I have added some .cmd file to the \_ISO\docs\E2B_Utilities\Force_Windows_Safe_Mode folder.

These are useful if you cannot get a system to go into Safe Mode.

These can be run from a live Windows 8/10 system or from WinPE (e.g. boot from a Windows 10 ISO and press SHIFT+F10 to get to the console)

They require Admin privileges (Run as Administrator):

EnableAdvancedOptionsBCD.cmd - modifies the current default Windows boot entry in the BCD so that on the next boot, the full Advanced list of boot options is presented. 
This allows you to choose Safe Mode boot, Boot with Logging, etc. 
This is a one-time only event, on the following reboot you will not see the Advanced Menu.

EnableSafeModeBootingBCD.cmd - modifies the current default Windows boot entry in the BCD so that it will always boot into Safe Mode. 
It also enables the Advanced Menu (once only) and sets Legacy Boot mode instead of the Windows bootmgr GUI boot menu. 
To undo the changes, you can run MSCONFIG once in Safe Mode, or run DisableSafeModeBootingBCD.cmd.

DisableSafeModeBootingBCD.cmd - Removes the Safe Mode boot and Legacy Boot Menu setting from the current default BCD.

Typically, if you have a system that you want to Safe Mode boot to, use the EnableAdvancedOptionsBCD.cmd script as this is the least invasive. 
You can then choose any of the options in the Windows pre-boot Advanced menu...

EnableSafeModeBootingBCD.cmd will always boot to Safe Mode, even if you don't choose a Safe Mode boot option in the Advanced Menu.