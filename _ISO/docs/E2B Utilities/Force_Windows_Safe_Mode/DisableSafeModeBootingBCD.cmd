@echo off
cls
color 1f
echo.
echo ****************************
echo *                          *
echo *    DISABLE SAFE MODE     *
echo *                          *
echo ****************************
echo.
bcdedit > nul
if errorlevel 1 set STORE=/store D:\boot\bcd

echo Press ENTER to run the 'bcdedit %STORE% /deletevalue {default} safeboot' command
echo.
echo Works from WinPE or Admin command prompt.
echo.
pause
echo.
bcdedit %STORE% /deletevalue {default} safeboot
if errorlevel 1 echo. & echo Operation failed! Please run as Administrator & echo.
pause
echo.
echo.
echo Press ENTER to run 'bcdedit %STORE% /deletevalue {default} bootmenupolicy' command
pause
echo.
bcdedit %STORE% /deletevalue {default} bootmenupolicy
if errorlevel 1 echo. & echo Operation failed! Please run as Administrator & echo.
pause
bcdedit %STORE% /enum {default} | more
pause

