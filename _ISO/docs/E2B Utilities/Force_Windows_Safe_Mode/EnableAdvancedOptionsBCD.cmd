@echo off
cls
color 1f
echo.
echo ****************************************
echo *                                      *
echo *      ENABLE ADVANCED BOOT MENU       *
echo *                                      *
echo *  Force Windows to show F8 Menu ONCE  *
echo *        on next boot only.            *
echo *                                      *
echo ****************************************
echo.
bcdedit > nul
if errorlevel 1 set STORE=/store D:\boot\bcd

echo.
echo Press ENTER to run 'bcdedit %STORE% /set {default} onetimeadvancedoptions on' command.
echo.
echo This forces Windows to display the Advanced Boot menu on the next boot.
echo.
echo Works from WinPE or Admin command prompt.
echo.
pause
echo.
bcdedit %STORE% /set {default} onetimeadvancedoptions on
if errorlevel 1 echo. & echo Operation failed! Please run as Administrator & echo.
pause
echo.
bcdedit %STORE% /enum {default} | more
pause
