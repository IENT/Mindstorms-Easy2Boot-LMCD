@echo off
cls
color 1f
echo.
echo ****************************************
echo *                                      *
echo *        ENABLE SAFE MODE BOOT         *
echo *                                      *
echo *  Force Windows to boot in Safe Mode  *
echo *                                      *
echo ****************************************
echo.
echo Press ENTER to run the 'bcdedit %STORE% /set {default} safeboot minimal' command.
echo.
echo Works from WinPE or Admin command prompt.
echo.
echo Tip: Run MSCONFIG to disable Safe Mode after booting.
echo.
pause
echo.
bcdedit %STORE% /set {default} safeboot minimal
if errorlevel 1 echo. & echo Operation failed! Please run as Administrator & echo.
pause


echo.
echo.
echo Press ENTER to run 'bcdedit %STORE% /set {default} onetimeadvancedoptions on' command.
echo.
echo This forces Windows to display the Advanced Boot menu on the next boot.
echo.
pause
echo.
bcdedit %STORE% /set {default} onetimeadvancedoptions on
if errorlevel 1 echo. & echo Operation failed! Please run as Administrator & echo.
pause


echo.
echo.
echo Press ENTER to run 'bcdedit %STORE% /set {default} bootmenupolicy legacy' command.
pause
echo.
bcdedit %STORE% /set {default} bootmenupolicy legacy
if errorlevel 1 echo. & echo Operation failed! Please run as Administrator & echo.
pause
echo.
bcdedit %STORE% /enum {default} | more
pause
