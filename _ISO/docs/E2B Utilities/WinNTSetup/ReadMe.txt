Download WinNTSetup 3.8.7 Beta 4 or later from http://www.msfn.org/ Forums
https://msfn.org/board/topic/149612-winntsetup-v41/

Extract and Copy all folders to here = \_ISO\docs\E2B Utilities\WinNTSetup

Now run the .exe file under Windows once to initialise it and then quit.

This folder also contains example diskpart .txt script files.

If the file .\WinNTSetup\tools\diskpart\enabled=1 exists, then you can press CTRL+SHIFT+D in WinNTSetup and run a diskpart script.

Tip: Move the whole WinNTSetup folder to the root of the E2B drive so that it is easier to find.

See http://www.easy2boot.com/add-payload-files/windows-install-isos/winntsetup/ for more details.
