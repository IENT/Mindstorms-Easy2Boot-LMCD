@echo off
set VER=0.07
:: History
:: 0.03 - change size, convert to jpg option
:: 0.04 - small text changes
:: 0.05 - get suggested size for IMA file automatically
:: 0.06 - tip to use 200 width added, fix code in copy ima file (last Q)
:: 0.07 - E2B v1.99 add extra line to .txt file and change 200 to 198

color 1f
mode con: cols=132 lines=60
setlocal EnableDelayedExpansion
pushd "%~dp0"
set SPAREVOL=Z:

echo.
echo %~n0 v%VER%    (c)2017 www.easy2boot.com [SSi] 
echo.
echo Converts an Animated GIF file to a .IMA floppy image file for use with Easy2Boot
echo.
echo.
echo Requirements
echo ============
echo Drive Z: must be free
echo ImDisk must be installed
if not exist "C:\Program Files (x86)\IrfanView\i_view32.exe" echo i_view32.exe (IrfanView executable) must be present in %~dp0 (or installed)
echo.
if not exist "%SystemRoot%\system32\IMDISK.EXE" echo. & echo ERROR: ImDisk not installed! Please read the ReadMe file. & color cf & pause & goto :EOF

if not exist "C:\Program Files (x86)\IrfanView\i_view32.exe" if not exist i_view32.exe color 4f & echo ERROR: i-view32.exe (IrfanView) not found!& pause & goto :EOF

if /i "%~1"=="Z" goto :DISZ
set GIF=%~1
if "%GIF%"=="" color 4f & echo. & echo ERROR: Please specify Animated GIF path or use drag-and-drop! & echo. & pause & goto :EOF
if /i not "%~x1"==".gif" color 4f & echo. & echo ERROR: %~nx1 is not a .GIF file! & echo. & pause & goto :EOF
if not exist "%GIF%" color 4f & echo ERROR: %GIF% does not exist! & pause & goto :EOF
if exist Z:\nul dir Z:\ & echo. & echo ERROR: Drive Z: is already in use!& echo. & echo If Z is an ImDisk drive, use %~n0 Z to dismount it & color 4f & set /p ask=Dismount (Y/N) : & if /i "!ask!"=="Y" goto :DISZ & goto :EOF

set IVIEW=i_view32.exe
if exist "C:\Program Files (x86)\IrfanView\i_view32.exe" set IVIEW="C:\Program Files (x86)\IrfanView\i_view32.exe"
echo.

:: check no spaces in filename - replace spaces with _ and check name is unchanged
set T=%~n1
if not "%T: =_%"=="%T%" color 4f & echo ERROR: Spaces are not allowed in the filename & pause & goto :EOF

set TARGET=
if defined TMP  if exist "%TMP%\*" set TARGET=%TMP%\GIFtoIMA
if defined TEMP if exist "%TEMP%\*" set TARGET=%TEMP%\GIFtoIMA
if "%TARGET%"=="" set TARGET=%~dp1GIFtoIMA


if not exist "%TARGET%\nul" mkdir "%TARGET%" 2> nul
if exist "%TARGET%\*.bmp" del "%TARGET%\*.bmp"

:: ---- IRFANVIEW EXTRACT -----
%IVIEW% "%GIF%"  /bpp=24 /extract=("%TARGET%",bmp) /cmdexit
:: remove unwanted files past 255
set NM=%TARGET%\%~n1_frame_
call :DELFILES
if "%E%"=="1" echo WARNING: Excess frames past %~n1_frame_0255.bmp were removed & pause & cls
echo Converting to 24-bit colour...
%IVIEW% "%TARGET%\*.bmp" /bpp=24 /convert="%TARGET%\*.bmp" /info="%TARGET%\info.txt"

echo.
echo FILES HAVE BEEN EXTRACTED TO %TARGET%
echo.
%WINDIR%\System32\find.exe  "Pixels" "%TARGET%\info.txt"
echo.
echo ============================================================================
echo     IMPORTANT: CHECK THAT EACH FRAME HAS EXACTLY THE SAME DIMENSIONS!
echo    Tip: Re-save GIF using KickMyGraphics v2.1 (loopback=off) to fix it.
echo         If unsure what size to use, try a Width of 198.
echo ============================================================================
echo.

:: ----- IRFANVIEW RESIZE --
echo         View and Resize
echo =================================
echo V = View slideshow in IrfanView
echo W = Width resize  (X)
echo H = Height resize (Y)
echo N = No change
:RESIZE
set ask=
echo.
set /p ask=Change Width, Height or View files (V/W/H/N) : 
if /i "%ask%"=="V" echo. & echo IrfanView SlideShow: - Use ^> and ^< cursor keys or ESC to exit & echo Re-save GIF using 'KickMyGraphics' (loopback mode=off) if some frames are not complete. & pause & (%IVIEW% /slideshow="%TARGET%\*.bmp" /closeslideshow) & goto :RESIZE
if /i not "%ask%"=="W" if /i not "%ask%"=="H" goto :GETSIZE
set W=0
set H=0
if /i "%ask%"=="W" set /p W=Width in pixels : & set /a W=!W!
if /i "%ask%"=="H" set /p H=Height in pixels : & set /a H=!H!
for %%f in ("%TARGET%\*.bmp") do (
%IVIEW% "%%f" /resize=^(%W%,%H%^) /resample /aspectratio /convert="%%f"  /cmdexit
)


:GETSIZE
:: convert to JPG
echo.
set ask=BMP
echo Y = JPG = Smaller size (transparent background not supported)
echo N = BMP = Transparent background supported
echo.
set /p ask=Convert to .JPG? (Y/[N]) : 
if /i not "%ask%"=="Y" set BMP=BMP
:: must use 100% quality for E2B
if /i "%ask%"=="Y" set BMP=JPG&& %IVIEW% "%TARGET%\*.BMP" /jpgq=100 /convert="%TARGET%\*.jpg"

cls
echo.
dir "%TARGET%\*.%BMP%"
echo.
echo.
:: Only works for English 'Files(s)'...
::dir "%TARGET%\*.%BMP%" | %WINDIR%\System32\find.exe /i "File(s)"
echo.
echo Note: If too small, press CTRL+C to abort and then increase the size by 2%%
echo.

set size=0
for %%x in ("%TARGET%\*.%BMP%") do set /a size+=%%~zx
set /a size=%size% / 1000
set /a MYSIZE=%size% + %size% / 30

echo SUGGESTED .IMA SIZE - if not enough space add approx. 5%% more (Total Files=%size%KB, .IMA Size=%MYSIZE%KB)
echo.
set /p MYSIZE=Size required for .IMA file (KB) [%MYSIZE%] : 
if "%MYSIZE%"=="" echo ERROR: Only use numbers! & pause & (cls & goto :GETSIZE)
set /a MYSIZE=%MYSIZE% + 0
if errorlevel 1 echo ERROR: Only use numbers! & pause & (cls & goto :GETSIZE)
if "%MYSIZE%"=="" echo ERROR: Only use numbers! & pause & (cls & goto :GETSIZE)
set /a MYSIZE=%MYSIZE% + 30
:: cannot be less than 68K
if %MYSIZE% LEQ 68 set MYSIZE=68

cls
set MYIMG=%~dp1%~n1.ima
echo.
echo Ready to run ImDisk and format %SPAREVOL%
echo.
echo MOUNT       = %SPAREVOL% 
echo SIZE        = %MYSIZE% 
echo FILE        = !MYIMG!
echo TEMP FOLDER = %TARGET%
echo.
::set /p ask=OK to continue ([Y]/N) : 
::if /i "%ask%"=="n" goto :EOF
::cls
echo.
echo Running ImDisk to create drive Z: - please wait...
if exist "!MYIMG!" del "!MYIMG!" > nul
echo Running command: imdisk -a -o fd -s %MYSIZE%k -m %SPAREVOL% -f "!MYIMG!" -p "/FS:FAT /Q /Y /V:GIF" 
imdisk -a -o fd -s %MYSIZE%k -m %SPAREVOL% -f "!MYIMG!" -p "/FS:FAT /Q /Y /V:GIF" > imdisk.log
if errorlevel 1 color 4f & type imdisk.log & echo ImDisk operation failed to mount !MYIMG!! & pause & goto :DISZ
md Z:\E2B > nul
echo.
echo Copying files to Z:\E2B...
echo.
echo If not enough space - use CTRL+C to abort
echo.
@echo off
echo "%TARGET%\*.!BMP!" Z:\E2B
xcopy "%TARGET%\*.!BMP!" Z:\E2B 

cls
echo.
echo.
dir /b Z:\E2B\
set CNT=0
:: count bmp files
@for /f %%a in ('2^>nul dir "%TARGET%\*.%BMP%" /a-d/b/-o/-p^|%WINDIR%\System32\find.exe /v /c ""') do set /a CNT=%%a
if %CNT% GEQ 256 set /a CNT=255 & Echo WARNING: Can only use up to %~n1_frame_0255.%BMP%
echo.

set TRANS=0x10
set ask=
if /i "%BMP%"=="BMP" set /p ask=Do you want the background to be transparent (Y/[N]) : 
if /i "%ask%"=="Y" set TRANS=0x90
echo.

echo MAKE A NOTE OF THE FIRST FILENAME AND NUMBER OF FILES
echo.
echo E2B will require these lines in MyE2B.cfg:
echo.
echo ===================================================
echo.
echo     set ANIMFD3=/_ISO/%~n1.ima
echo     set ANIMATE=%TRANS%=3=%CNT%=575=225 (fd3)/E2B/%~n1_frame_0001.%BMP%
echo.
echo ===================================================
echo.
echo     LAST FRAME NUMBER IS %CNT%

: Make TXT file
echo.> "%~dp1%~n1.txt"
echo set ANIMFD3=/_ISO/%~n1.ima>> "%~dp1%~n1.txt"
(echo set last=%CNT%)>> "%~dp1%~n1.txt"
echo # 1st param - use 0x90 for looping+transparent background, use 0x10 for opaque background>> "%~dp1%~n1.txt"
echo # 2nd param is delay (1\18th second), 3rd is last frame number, 4th is x pos, 5th is y pos (in pixels), 6th is path to first frame>> "%~dp1%~n1.txt"
echo set ANIMATE=%TRANS%=3=%%last%%=575=225 (fd3)/E2B/%~n1_frame_0001.%BMP%>> "%~dp1%~n1.txt"
echo.

:: clean up - delete folder
if exist "%TARGET%\*.*" rmdir "%TARGET%" /s /Q

:DISZ
echo Dismounting %SPAREVOL%...
IMDISK.EXE -d -m %SPAREVOL% >nul 2>&1
if errorlevel 1 echo WARNING: Cannot dismount volume %SPAREVOL% - Please close all open files and all Explorer windows on %SPAREVOL% & if not "%AUTORUN%"=="Y" pause
if errorlevel 1 IMDISK.EXE -d -m %SPAREVOL% >nul 2>&1
if errorlevel 1 echo Forcing Volume to dismount & IMDISK.EXE -D -m %SPAREVOL% >nul 2>&1 & if errorlevel 1 echo WARNING: IMAGE MAY BE CORRUPT! & start imdisk.cpl
if exist %SPAREVOL%\nul echo ERROR: IMAGE WAS NOT DISMOUNTED!
if exist %SPAREVOL%\nul echo        USE CONTROL PANEL - IMDISK to remove the ramdrive %SPAREVOL% & start imdisk.cpl & color 4f
echo.
if exist "!MYIMG!" (
	echo !MYIMG! created OK. & color 2f
	echo.
	echo %~dp1%~n1.txt contains these lines for use in \_ISO\MyE2B.cfg file...
	echo.
	echo ===================================================
	type "%~dp1%~n1.txt"
	echo ===================================================
	if exist imdisk.log del imdisk.log
	)
@echo.
@echo.

:: -- ADD to MyE2B.cfg ---

if exist %~d0\_ISO\e2b\grub\E2B_grub.txt (
	set ask=N
	set /p ask="Add lines to end of %~d0\_ISO\MyE2B.cfg and copy !MYIMG! to %~d0\_ISO folder? (Y/[N]) : "
	if /i "!ask!"=="Y" (
		if exist %~d0\_ISO\MyE2B.cfg type "%~dp1%~n1.txt" >> %~d0\_ISO\MyE2B.cfg
		if not exist %~d0\_ISO\MyE2B.cfg (
			echo ^^!BAT > %~d0\_ISO\MyE2B.cfg
			type "%~dp1%~n1.txt" >> %~d0\_ISO\MyE2B.cfg 
			)
		echo copying !MYIMG! to %~d0\_ISO...
		copy "!MYIMG!" %~d0\_ISO 2>nul
	)
)

@echo.
pause
goto :EOF

:: --------------- END -----------------------

:GETTARG
set TARGET=%~dp1%2
goto :EOF


:DELFILES
:: %~n1_frame_0001.bmp
:: once exit /b is run, no more commands are executed but count will still continue
set E=0
FOR /L %%G IN (256,1,999) DO (
if "!E!"=="1" if not exist "%NM%0%%G.bmp" exit /b
if exist "%NM%0%%G.bmp" set E=1& echo DELETING %NM%0%%G.bmp & del "%NM%0%%G.bmp"
)
goto :EOF
