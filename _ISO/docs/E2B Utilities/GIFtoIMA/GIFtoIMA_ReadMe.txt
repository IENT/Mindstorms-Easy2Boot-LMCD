GIFtoIMA
========

This script converts an Animated .GIF file to a .IMA image file for use with Easy2Boot
It also creates a .txt file with the required lines for your \_ISO\MyE2B.cfg file.

Horse.GIF  >>>>   Horse.IMA  + Horse.txt

http://www.easy2boot.com/configuring-e2b/animation/

Use the free utility KickMyGraphics to convert any GIF to the correct format (set loopback mode=off). 
http://download.cnet.com/KickMyGraphics/3000-2186_4-75864020.html


USAGE
=====
Drag-and-drop an Animated GIF file onto the GIFtoIMA.cmd file
If the GIF is of the 'difference' type use the free app KickMyGraphics to save a GIF in the correct format (loopback mode=off). http://download.cnet.com/KickMyGraphics/3000-2186_4-75864020.html

Multiple .BMP files will be extracted from the .GIF file. These will be listed on the display.
You must check the X and Y dimensions are the same for all frames (they don't need to be square).
Some GIFs only store differences. These are not suitable for E2B. 
Check this using the V=View slideshow option.
You can convert to a different size by changing H=height or W=width (aspect ratio is preserved).
You can convert the JPG for smaller file size, but JPGs do not work well if using a transparent background.
Enter the total size required for the image file by looking at the directory 'Files(s)' number (use CTRL+C if there is a problem)
You may need to increase the size by approx 10% for some larger GIFs (I add 30KB by default).
ImDisk will then make a virtual Z: drive, copy the files to an E2B sub-folder and then dismount the drive.
The .IMA output file and the .txt output file will be written to the same folder as the .GIF file.
You can also choose to copy the .IMA file to the \_ISO and add the code lines to \_ISO\MyE2B.cfg.

The .txt file will contain the essential information for \_ISO\MyE2B.cfg, or if you prefer, 
you can cut-and-paste the text shown on the screen into your \_ISO\MyE2B.cfg file.

Note: The .bmp/.jpg frames will always be placed in a \E2B folder inside the floppy .IMA image file.

Tip: To reduce the size of the final .ima file, drag-and-drop it onto \_ISO\docs\E2B Utilities\LZMA\LZMA_ENCODE.cmd to compress it.


Typical .txt file
=================
set ANIMFD3=/_ISO/lightning.ima
set last=10
# 1st param - use 0x90 for looping+transparent background, use 0x10 for opaque background
# 2nd param is delay, 3rd is last frame number, 4th is x pos, 5th is y pos (in pixels), 6th is path to first frame
set ANIMATE=0x90=3=%last%=575=225 (fd3)/E2B/lightning_frame_0001.bmp

This can be pasted into your \_ISO\MyE2B.cfg file directly.
You must also copy the IMA file to the \_ISO folder.

See http://www.easy2boot.com/configuring-e2b/animation/ for more information.