@echo off

:BEGIN
cls
color 1f
echo *************************************
echo *                                   *
echo *    Make \_ISO\CONTIG.ISO file     *
echo *                                   *
echo *************************************
echo.
set USB=%~d0
if not exist %USB%\_ISO\e2b\grub\menu.lst (color 4f & echo ERROR: Please run this from the E2B USB drive! & pause & goto :EOF)
set SIZE=500
set /p SIZE=Enter size for new \_ISO\CONTIG.ISO file in MB (default=500) : 
if "%SIZE:~0,1%"=="0" (echo ERROR: %SIZE% is not a valid number & pause & goto :BEGIN)
set /A "b=%SIZE%+0"2>nul || (echo ERROR: %SIZE% is not a valid number & pause & goto :BEGIN)
set ask=N
echo.
if exist %USB%\_ISO\CONTIG.ISO set /p ask=OK to delete existing %USB%\_ISO\CONTIG.ISO (Y/[N]) : 
if /i "%ask%"=="Y" if exist %USB%\_ISO\CONTIG.ISO del %USB%\_ISO\CONTIG.ISO
if exist %USB%\_ISO\CONTIG.ISO goto :EOF

echo Creating %SIZE%000000 byte file %USB%\_ISO\CONTIG.ISO... 
color 2f
fsutil file createnew %USB%\_ISO\CONTIG.ISO %SIZE%000000 && set OK=1
if errorlevel 1 color 4f
echo.
if "%OK%"=="1" echo Now run \MAKE_THIS_DRIVE_CONTIGUOUS.cmd to make the file contiguous.
echo.
pause
