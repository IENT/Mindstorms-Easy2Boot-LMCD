POS 2009 Embedded ISO

https://www.microsoft.com/en-us/download/details.aspx?id=11196

Read the help file in the \Documentation folder inside the ISO.

Here are instructions for adding POS2009.iso to Easy2Boot.

Install only Product Key:  D4GQ7-HG48K-7YP6R-6JM4C-33FMQ

System should have 1GB+ of RAM
Use F8/F10 BIOS Boot Selection menu key to boot from E2B USB (do not set boot order in BIOS Setup to boot from USB)

Tip: Use the Setup Wizard's Full Install option to ensure all drivers are added. Also add the extra optional Components as many standard utilities are missing from the default install.

Systems with IDE only HDD
=========================

1. Extract all FOLDERS from ISO to root of Easy2Boot USB drive
E2B USB drive should contain folders:

\Setup         (not required, approx. 160MB)
\Utilities
\Program Files
\I386
\Documentation (not required)

Note: Mass Storage Drivers can be added by creating a \Drivers\Manufname\ folder and copying the drivers to it. Then press F6 when prompted on first boot.

2. Extract all \win*.* FILES to the root of the Easy2Boot USB drive
3. Copy POS2009.iso to the \_ISO\MainMenu folder and rename as POS2009.isowinv

To use:
1. Select POS2009.isowinv from Easy2Boot main menu
2. Allow to boot to POS setup and enter product key and continue install
3. When it reboots - allow to boot from the internal hard disk (keep Easy2Boot USB drive connected)

Use USB 2.0 port (not USB 3 or modern mainboard).

Logon with username = Administrator   Password (as specified previously).

SATA\RAID\SCSI target system
============================

Must use the DPMS version of E2B.

1. Extract FOLDERS from ISO to root of Easy2Boot USB drive (see above)
2. Extract all \win*.* FILES to the root of the Easy2Boot USB drive
3. Copy POS2009.iso to the \_ISO\WINDOWS\XP folder

To use:
1. Select POS2009.iso from Easy2Boot XP Install Windows menu - XP Install Step 1 - use default options.
2. Allow to boot to POS setup and enter POS2009 product key and continue install
IMPORTANT: Tick the 'Add Third-Party Driver' option and select the same DPMS driver that was installed by Easy2Boot previously (typically the first driver in list).
3. When it reboots - allow to boot from the internal hard disk (keep Easy2Boot USB drive connected)
