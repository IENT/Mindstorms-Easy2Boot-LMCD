@echo off
TITLE %~n0 v1.3
:: set cmd shell window size
mode con: cols=60 lines=20 2> nul
color 1f
echo.
set MYPATH=
set "WPATH=%~dp0"
if exist "%WPATH%\WinContig.exe" set "MYPATH=%WPATH%"
set "WPATH=%~d0\_ISO\docs\WINCONTIG"
if exist "%WPATH%\WinContig.exe" set "MYPATH=%WPATH%"

set "P1=%MYPATH%\MAKE_ROOT_FOLDER_CONTIGUOUS.cmd" 
if exist "%P1%" call "%P1%" || echo ERROR: %P1% Not found! && pause
set "P1=%MYPATH%\WINCONTIG\MAKE_ISO_FOLDER_CONTIGUOUS.cmd " 
if exist "%P1%" call "%P1%"  || echo ERROR: %P1% Not found! && pause





