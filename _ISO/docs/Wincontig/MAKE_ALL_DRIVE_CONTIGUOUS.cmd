@echo off

TITLE %~n0
:: set cmd shell window size
mode con: cols=120 lines=10 2> nul
::if not exist %~d0\_ISO\e2b\grub\menu.lst echo ERROR: Please run %~nx0 from the E2B drive. & color 4f & pause & goto :EOF
set MYPATH=
set "WPATH=%~dp0"
if exist "%WPATH%\WinContig.exe" set "MYPATH=%WPATH%"
set "WPATH=%~d0\_ISO\docs\WINCONTIG"
if exist "%WPATH%\WinContig.exe" set "MYPATH=%WPATH%"

set "WC=%MYPATH%\WinContig.exe"
if not exist %WC% echo ERROR: Cannot find WinContig on this drive! & pause & goto :EOF
pushd "%~dp0%"
echo Making all files on %~d0 contiguous - please wait...
echo %WC% "%~d0" /DEFRAG /CLOSEIFOK /CLEAN:0 /LANG:Auto /NOINI /QUICK 
%WC% "%~d0" /DEFRAG /CLOSEIFOK  /CLEAN:0 /LANG:Auto /NOINI /QUICK 
