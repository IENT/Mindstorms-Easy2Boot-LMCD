Usage: wtee [OPTION]... [FILE]...
Copy standard input to each FILE, and also to standard output

-a               append to the given FILEs, do not overwrite
-i               ignore interrupt signals
-?, --help       display this help and exit
--version        output version information and exit

If a FILE is -, copy again to standard output

Report bugs to rbuhl@point2.com

