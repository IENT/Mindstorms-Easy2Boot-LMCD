   4 ;;   Copyright 1994-2008 H. Peter Anvin - All Rights Reserved
   5 ;;   Copyright 2009 Intel Corporation; author: H. Peter Anvin
   6 ;;
   7 ;;   This program is free software; you can redistribute it and/or modify
   8 ;;   it under the terms of the GNU General Public License as published by
   9 ;;   the Free Software Foundation, Inc., 53 Temple Place Ste 330,
  10 ;;   Boston MA 02111-1307, USA; either version 2 of the License, or
  11 ;;   (at your option) any later version; incorporated herein by reference.
  12 ;;
  13 ;; -----------------------------------------------------------------------
 
https://repo.or.cz/syslinux.git/tree/HEAD:/memdisk
