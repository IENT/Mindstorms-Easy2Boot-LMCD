NTBOOT is a grub4dos script originally written by chenall

It is used by the E2B menu system to boot to .wim and .vhd Windows files.

It has been modified by Steve Si to improve it.

It is in plain text and so the source code is available.

Chenall requests that his code should not be used for commercial purposes.

http://chenall.net/post/ntboot/

https://plus.google.com/100700322385676715564