@echo off
mode con: cols=100 lines=40
color 1f
cls

start control userpasswords2
echo.
echo.
net user ADMIN admin /add 2> nul
net localgroup administrators ADMIN /add  2> nul
net user .\ADMIN admin /add  2> nul
net localgroup administrators .\ADMIN /add  2> nul
echo.
net user
echo.
echo.
net user | find "ADMIN" > nul && (
echo OK: ADMIN account has been created.
echo.
echo You may need to reboot or use sleep/wake to log in:
echo.
echo ----------------------------
echo        USERNAME: ADMIN
echo        PASSWORD: admin
echo ----------------------------
echo.
echo.
echo To remove ADMIN account: At the login screen, type WIN+U then 3.
echo.
)
net user | find "ADMIN" > nul || color 4f && echo ERROR: ADMIN account was not made. Use 'Run as Administrator'!
echo.
pause
taskkill.exe /F /IM LogonUI.exe

