@echo off
mode con: cols=100 lines=40
color 1f
SETLOCAL EnableDelayedExpansion
cls
for %%G in (C D E F G H I J K L M N O P Q R S T U V W X Y Z) DO (
if exist %%G:\WINDOWS\SYSTEM32\UTILMAN.EXE.BAK copy %%G:\WINDOWS\SYSTEM32\UTILMAN.EXE.BAK %%G:\WINDOWS\SYSTEM32\UTILMAN.EXE || set ERR=1
if exist %%G:\WINDOWS\SYSTEM32\SETHC.EXE.BAK copy %%G:\WINDOWS\SYSTEM32\SETHC.EXE.BAK     %%G:\WINDOWS\SYSTEM32\SETHC.EXE   || set ERRS=1

REM If running from Windows (not WinPE) try to restore files
if exist %%G:\WINDOWS\SYSTEM32\UTILMAN.EXE echo Checking UTILMAN.EXE on %%G: & sfc /offbootdir=%%G:\ /offwindir=%%G:\windows /scanfile=%%G:\windows\system32\utilman.exe
if exist %%G:\WINDOWS\SYSTEM32\SETHC.EXE echo Checking SETHC.EXE on %%G: & sfc /offbootdir=%%G:\ /offwindir=%%G:\windows /scanfile=%%G:\windows\system32\sethc.exe

if exist %%G:\WINDOWS\SYSTEM32\UTILMAN.EXE echo. && echo DRIVE %%G: & dir %%G:\WINDOWS\SYSTEM32\UTILMAN.EXE* | find /i "exe"
if exist %%G:\WINDOWS\SYSTEM32\SETHC.EXE dir %%G:\WINDOWS\SYSTEM32\SETHC.EXE* | find /i "exe"
if exist %%G:\WINDOWS\SYSTEM32\UTILMAN.EXE dir %%G:\WINDOWS\SYSTEM32\CMD.EXE | find /i "exe"
if exist %%G:\WINDOWS\SYSTEM32\SETHC.EXE dir %%G:\WINDOWS\SYSTEM32\CMD.EXE | find /i "exe"
set ASK=
if exist %%G:\USERS\ADMIN\nul echo. && set /p ASK=Press Y to delete the %%G:\USERS\ADMIN account folder ^(Y/[N]^) : 
if /i "!ASK!"=="Y" rd %%G:\USERS\ADMIN /Q /S
if /i "!ASK!"=="Y" if not exist %%G:\USERS\ADMIN\nul echo %%G:\USERS\ADMIN user account folder was deleted OK && echo Tip: Use 'rd %%G:\USERS\xxx /S' to delete more folders && echo.
if /i "!ASK!"=="Y" echo %%G:\USERS folders && echo ================ &&  dir %%G:\USERS /ad-l /b
set ASK=
if exist %%G:\WINDOWS\SYSTEM32\SETHC.EXE pause
)
echo.

if not "%ERR%"==""  echo POSSIBLE PROBLEMS RESTORING UTILMAN! Run with Admin rights (WIN+X - Powershell (Admin) - cmd.exe) or run under WinPE! && color 4f
if not "%ERRS%"=="" echo POSSIBLE PROBLEMS RESTORING SETHC!   Run with Admin rights (WIN+X - Powershell (Admin) - cmd.exe) or run under WinPE! && color 4f
@echo.
@echo CHECK WINDOWS IS UNPATCHED: 
@echo Reboot to Windows and use WIN+U and [SHIFT] x5 and make sure you do not get the cmd shell now...
@echo.
@echo.
pause



