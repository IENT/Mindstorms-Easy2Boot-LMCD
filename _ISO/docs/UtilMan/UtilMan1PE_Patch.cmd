@echo off
mode con: cols=100 lines=40
color 1f
echo *************************************
echo *                                   *
echo *  PATCH UTILMAN.exe and SETHC.exe  *
echo *                                   *
echo *    on all copies of Windows       *
echo *                                   *
echo *************************************
echo.
pause

set ERR=
for %%G in (C D E F G H I J K L M N O P Q R S T U V W X Y Z) DO (
if exist %%G:\WINDOWS\SYSTEM32\UTILMAN.EXE if not exist %%G:\WINDOWS\SYSTEM32\UTILMAN.EXE.BAK copy %%G:\WINDOWS\SYSTEM32\UTILMAN.EXE %%G:\WINDOWS\SYSTEM32\UTILMAN.EXE.BAK
if exist %%G:\WINDOWS\SYSTEM32\SETHC.EXE   if not exist %%G:\WINDOWS\SYSTEM32\SETHC.EXE.BAK copy %%G:\WINDOWS\SYSTEM32\SETHC.EXE   %%G:\WINDOWS\SYSTEM32\SETHC.EXE.BAK
if exist %%G:\WINDOWS\SYSTEM32\CMD.exe copy %%G:\WINDOWS\SYSTEM32\CMD.EXE     %%G:\WINDOWS\SYSTEM32\UTILMAN.EXE
if exist %%G:\WINDOWS\SYSTEM32\CMD.exe copy %%G:\WINDOWS\SYSTEM32\CMD.EXE     %%G:\WINDOWS\SYSTEM32\SETHC.EXE

if exist %%G:\WINDOWS\SYSTEM32\UTILMAN.EXE echo. && echo DRIVE %%G: & dir %%G:\WINDOWS\SYSTEM32\UTILMAN.EXE* | find /i "exe"
if exist %%G:\WINDOWS\SYSTEM32\SETHC.EXE dir %%G:\WINDOWS\SYSTEM32\SETHC.EXE* | find /i "exe"
if exist %%G:\WINDOWS\SYSTEM32\UTILMAN.EXE dir %%G:\WINDOWS\SYSTEM32\CMD.EXE | find /i "exe"
if exist %%G:\WINDOWS\SYSTEM32\SETHC.EXE dir %%G:\WINDOWS\SYSTEM32\CMD.EXE | find /i "exe"

if exist %%G:\WINDOWS\SYSTEM32\UTILMAN.EXE copy "%~dp0Utilman2*.cmd" %%G:\WINDOWS\SYSTEM32\2.cmd > nul
if exist %%G:\WINDOWS\SYSTEM32\SETHC.EXE copy "%~dp0Utilman2*.cmd" %%G:\WINDOWS\SYSTEM32\2.cmd > nul
if exist %%G:\WINDOWS\SYSTEM32\UTILMAN.EXE copy "%~dp0Utilman3*.cmd" %%G:\WINDOWS\SYSTEM32\3.cmd > nul
if exist %%G:\WINDOWS\SYSTEM32\SETHC.EXE copy "%~dp0Utilman3*.cmd" %%G:\WINDOWS\SYSTEM32\3.cmd > nul

)
if "%ERR%"=="" (
color 2f
@echo.
@echo Now boot to Windows in SAFE MODE
@echo Press WIN+U to get a command prompt OR press SHIFT key 5 times
@echo Then type 2 [ENTER]  to run C:\Windows\system32\2.cmd and create an ADMIN account.
@echo.
@echo Then reboot and login as User=ADMIN  Password=admin
@echo.
@echo.
@echo Press ENTER and reboot in SAFE MODE and press WIN+U quickly...
pause > nul
wpeutil reboot
shutdown -t 2 /r
)
if "%ERR%"=="1" (
color 4f
echo.
echo ERROR: AT LEAST ONE FILE WAS NOT REPLACED WITH CMD.EXE!
echo.
)
pause


