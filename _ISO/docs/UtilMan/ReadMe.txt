As you may know, if you have not encrypted your Windows OS with BitLocker, it is quite easy to get into your OS by using the trick of replacing UtilMan.exe with Cmd.exe and\or SetHC.exe with Cmd.exe.

Once you have done this, you can hit WINKEY+U or press the SHIFT key five times to bring up a Windows cmd shell box with full admin rights.

To semi-automate this process and save some typing, E2B now contains a new \_ISO\docs\utilman folder which contains four .cmd script files:

UtilMan1PE_Patch.cmd               - replaces UtilMan.exe and Sethc.exe with Cmd.exe
UtilMan2Win_MakeAdmin.cmd (2.cmd)  - creates a new 'ADMIN' user account (password 'admin')
UtilMan3Win_DelAdmin.cmd  (3.cmd)  - deletes the ADMIN user account
UtilMan4PE_Restore.cmd             - restores UtilMan.exe and Sethc.exe

Here is how to use them:

Requirements
============

1. E2B must be on a Removable USB Flash drive (or add an E2B WinHelper drive or boot using WIMBOOT)
2. Copy a Windows 10 Home or Professional Install ISO to the \_ISO\WINDOWS\WIN10 folder


Method
======

First boot to Windows and hold down the SHIFT key and click Restart
Choose Troubleshoot - Advanced - Startup - Restart.

1. Use the E2B Windows 10 install menu to select the Win10 ISO file. 
Choose the 'Utilman - Hack Windows (UtilMan.exe).xml' file. 
Note that this will patch any version of Windows that it finds on any valid partition (as long as Windows uses the \Windows folder).
It also copies and renames the Windows scripts to C:\WINDOWS\SYSTEM32\2.cmd and 3.cmd for later use.

2. Reboot to Windows and at the login screen...
Press WIN+U quickly or the SHIFT key five times - you should see a windows command prompt open up.
Now type 2 quickly and press [ENTER] to add a new ADMIN account (UtilMan2Win_MakeAdmin.cmd).

You have approx. 30 seconds to type WIN+U and 2 [ENTER] before the hotkey will be disabled by Windows Defender!

At this point, you may need to reboot to Windows again in order to be able to log in as ADMIN. 
A quicker alternative is to choose the power option 'Sleep' if available and then press the power button or a key to wake up the computer to see the ADMIN user choice.

Now log in as ADMIN (password=admin) and do whatever you want to do!

3. Log out of Windows and press WIN+U (or SHIFT 5 times) at the Login screen.
Now type 3 and press [ENTER] to run 3.cmd (UtilMan3Win_DelAdmin.cmd) to delete the ADMIN account that was created previously.
Depending on your version of Windows, it may also restore the original Utilman.exe and SetHC.exe files using SFC.

4. To 'Unhack' the files
Use the E2B Windows 10 install menu to select the Win10 ISO file. 
Choose the 'Utilman - UnHack Windows (restore UtilMan.exe).xml' file.

The backup files xxxxxx.exe.bak will not be deleted.
C:\Windows\System32\2.cmd and 3.cmd will not be deleted.

You will need Administrator privileges for these scripts to work and suitable permissions.

Note that when running the patched versions of UtilMan and SetHC, any command shell error messages will not be able to be translated correctly 
because the UtilMan.exe.mui and SetHC.exe.mui files will not be correct.