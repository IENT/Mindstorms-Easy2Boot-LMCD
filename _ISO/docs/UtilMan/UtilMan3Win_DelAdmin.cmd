@echo off
mode con: cols=100 lines=40
color 1f
cls

REM Windows 10 seems unable to repair using SFC due to bug in recent versions???
echo Running SFC to restore utilman.exe and sethc.exe - please wait...
sfc /scanfile=C:\windows\system32\utilman.exe > nul
sfc /scanfile=C:\windows\system32\sethc.exe > nul
@echo.

dir C:\Windows\System32\Utilman.exe | find "exe"
dir C:\Windows\System32\SetHC.exe | find "exe"
dir C:\Windows\System32\cmd.exe | find "exe"
@echo.

net user ADMIN /del 2> nul
net user .\ADMIN /del 2> nul
net user

net user | find  "ADMIN" > nul || echo ADMIN account has been deleted OK & color 2f
net user | find  "ADMIN" > nul && (echo ERROR: ADMIN ACCOUNT WAS NOT DELETED - must run from Login screen.'! && color 4f)
echo.

if exist C:\Windows\System32\UtilMan.exe fc /b C:\Windows\System32\UtilMan.exe C:\Windows\System32\cmd.exe > nul && echo WARNING: UtilMan.exe IS STILL PATCHED! && set P=1
if exist C:\Windows\System32\SetHC.exe   fc /b C:\Windows\System32\SetHC.exe   C:\Windows\System32\cmd.exe > nul && echo WARNING: SetHC.exe   IS STILL PATCHED! && set P=1
if "%P%"=="1" echo. && echo You should UnHack Windows by running UtilMan4PE_Restore.cmd! & echo. && color 4f

if exist C:\USERS\ADMIN echo WARNING: C:\USERS\ADMIN folder still exists!
echo.
echo.
echo Now use a Windows 10 ISO + 'Utilman - UnHack Windows (restore UtilMan.exe).XML'
echo to boot to WinPE and run UtilMan4PE_Restore.cmd.
echo.