@echo off
TITLE %~nx0 (start_setup.bat)
wpeinit.exe
REM prevent wpeinit from running again
ren X:\windows\system32\wpeinit.exe wpeinit.exe.old

for %%I in (C D E F G H I J K L M N O P Q R S T U V W X Y Z) do if exist %%I:\_ISO\e2b\grub\e2b.cfg set E2BDRIVE=%%I:
IF "%E2BDRIVE%"=="" (
echo ERROR - COULD NOT FIND E2B DRIVE!
echo Note: Windows 7 does not contain USB 3 drivers or modern USB 2.0 chipset drivers.
echo LIST DISK | diskpart
echo LIST VOL | diskpart
pause
goto :EOF
)

cd %E2BDRIVE%\
%E2BDRIVE%
call \_ISO\e2b\firadisk\loadisonp.cmd

TITLE %~nx0 (start_setup.bat)
%DVDDRIVE%
cd \

REM X:\Setup.exe will ask for Repair, X:\Sources\Setup.exe will just run installer
REM inject XML to be picked up by Setup.exe
if exist X:\Windows\System32\AutoUnattend.xml copy X:\Windows\System32\AutoUnattend.xml X:\

MODE CON COLS=30 LINES=2
echo DO NOT CLOSE THIS WINDOW
X:\setup.exe || echo trying \Sources\Setup.exe & X:\Sources\setup.exe
