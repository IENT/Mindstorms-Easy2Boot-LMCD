@echo off
color 1f
TITLE %~dpnx0 (E2B-WIMBOOT start_WinNTSetup.bat)
wpeinit.exe
REM prevent wpeinit from running again
ren X:\windows\system32\wpeinit.exe wpeinit.exe.old

for %%I in (C D E F G H I J K L M N O P Q R S T U V W X Y Z) do if exist %%I:\_ISO\e2b\grub\e2b.cfg set E2BDRIVE=%%I:
IF "%E2BDRIVE%"=="" (
echo ERROR - COULD NOT FIND E2B DRIVE!
pause
goto :EOF
)

cd %E2BDRIVE%\
%E2BDRIVE%
REM get the naame of the ISO
if exist %E2BDRIVE%\_ISO\e2b\firadisk\isoname.cmd call %E2BDRIVE%\_ISO\e2b\firadisk\isoname.cmd
set pp=_x86
if /i "%PROCESSOR_ARCHITECTURE%"=="AMD64" set pp=_x64
set wpath=%E2BDRIVE%\WinNTSetup
if not exist "%wpath%\WinNTSetup%pp%.exe" set wpath=%E2BDRIVE%\_ISO\docs\E2B Utilities\WinNTSetup
if not exist "%wpath%\WinNTSetup%pp%.exe" echo ERROR: WinNTSetup%pp%.exe not found on %E2BDRIVE% & pause
MODE CON COLS=30 LINES=2
echo DO NOT CLOSE THIS WINDOW

REM Example to select an XML file and an Edition index number
REM "%wpath%\WinNTSetup%pp%.exe" NT6 -source:"%MYISO%" -wimindex:"2" -unattend:"Win10ProUK_with_SDI_CHOCO.xml" -sysPart:C: -tempDrive:C:
REM Example to use .cfg file (you can save a .cfg file using Ctrl+S)
REM "%wpath%\WinNTSetup%pp%.exe" NT6 -source:"%MYISO%" -wimindex:"5" -cfg:WinNTSetupSpecial.ini

REM Load the first image in the install.wim because we don't know which one the user wants
"%wpath%\WinNTSetup%pp%.exe" NT6 -source:"%MYISO%" -wimindex:"1"
start /w cmd
