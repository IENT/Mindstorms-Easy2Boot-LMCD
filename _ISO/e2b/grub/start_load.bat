@echo off
TITLE %~dpnx0 (E2B-WIMBOOT start_load.bat)
wpeinit.exe
REM prevent wpeinit from running again
ren X:\windows\system32\wpeinit.exe wpeinit.exe.old

for %%I in (C D E F G H I J K L M N O P Q R S T U V W X Y Z) do if exist %%I:\_ISO\e2b\grub\e2b.cfg set E2BDRIVE=%%I:
IF "%E2BDRIVE%"=="" (
echo ERROR - COULD NOT FIND E2B DRIVE!
pause
)

cd %E2BDRIVE%\
%E2BDRIVE%
call \_ISO\e2b\firadisk\loadisonp.cmd
REM startnet.cmd normally only contains wpeinit.exe, custom ISOs may contain other commands, so try to run them
TITLE %~dpnx0 (E2B-WIMBOOT start_load.bat)
if exist X:\WINDOWS\SYSTEM32\startnet.cmd call cmd /c  X:\WINDOWS\SYSTEM32\startnet.cmd

cls
if defined E2BDRIVE echo E2BDRIVE=%E2BDRIVE%
if defined DVDDRIVE echo DVDDRIVE=%DVDDRIVE%
echo.
%DVDDRIVE% > nul
cd \ > nul

REM MODE CON COLS=30 LINES=2
REM echo DO NOT CLOSE THIS WINDOW
REM pause > nul

if not  exist X:\Setup.exe goto :skps
set ask=Y
set /p ask=Run Setup.exe (Y/[N]) : 
if /i "%ask%"=="Y" X:\Setup.exe
goto :fin

:skps
if not  exist X:\Sources\Recovery\RecEnv.exe goto :skpr
set ask=Y
set /p ask=Run Windows Recovery (Y/[N]) : 
if /i "%ask%"=="Y" X:\Sources\Recovery\RecEnv.exe
:skpr

:fin
echo.
call cmd /k
echo.
echo WARNING: About to close and exit WinPE...
pause
