@echo off
TITLE %~nx0 (E2B-WIMBOOT startup_winnt.bat)
wpeinit.exe
REM prevent wpeinit from running again
ren X:\windows\system32\wpeinit.exe wpeinit.exe.old

for %%I in (C D E F G H I J K L M N O P Q R S T U V W X Y Z) do if exist %%I:\_ISO\e2b\grub\e2b.cfg set E2BDRIVE=%%I:
IF "%E2BDRIVE%"=="" (
echo ERROR - COULD NOT FIND E2B DRIVE!
echo Note: Windows 7 does not contain USB 3 drivers or modern USB 2.0 chipset drivers.
echo LIST DISK | diskpart
echo LIST VOL | diskpart
pause
goto :EOF
)

cd %E2BDRIVE%\
%E2BDRIVE%
call \_ISO\e2b\firadisk\loadisoW.cmd
TITLE %~nx0 (E2B-WIMBOOT startup_winnt.bat)

%DVDDRIVE%
cd \

MODE CON COLS=30 LINES=2
echo DO NOT CLOSE THIS WINDOW
REM X:\setup.exe /Unattend:%E2BDRIVE%\AutoUnattend.xml
pause startup_winnt.bat ending...