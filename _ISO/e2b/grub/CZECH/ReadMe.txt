Pou�it� vlastn�ho jazyka

1. Zkop�rujte \_ISO\e2b\grub\ENG slo�ku do nov� slo�ky - nap�. \_ISO\e2b\grub\FRED
2. Upravte STRINGS.txt a F1.cfg soubory v nov� slo�ce
3. V \_ISO\MyE2B.cfg (kopii \_ISO\Sample_MyE2B.cfg)  souboru LANG=FRED a zm�nit z�hlav�, z�pat� HELPTEXT atd. podle pot�eby
4. Chcete-li zm�nit textov� zpr�vy v \_ISO\e2b\grub\Menu.lst  pak jej zkop�rujte do \_ISO\e2b\grub\FRED\Menu.lst (nedoporu�eno)

Pozn�mka: Pokud /_ISO/STRINGS.txt existuje, bude v�dy pou�it (Jazyk je ignorov�n)
	     
Koukn�te na www.easy2boot.com pro v�ce informac�
