@echo off
REM Look for Make_E2B_USB_Drive.cmd
REM This file can be moved to _ISO folder or \_ISO\docs folder

if exist "%~dp0_ISO\docs\Make_E2B_USB_Drive\Make_E2B_USB_Drive.cmd" set PP=%~dp0_ISO\docs\Make_E2B_USB_Drive\Make_E2B_USB_Drive.cmd
if not "%1"=="am_admin" call powershell -h | %WINDIR%\System32\find.exe /i "powershell" > nul && if not "%1"=="am_admin" (powershell start -verb runas '%0' am_admin > nul & exit)
if "%PP%"=="" if exist "%~dp0docs\Make_E2B_USB_Drive\Make_E2B_USB_Drive.cmd" set PP=%~dp0docs\Make_E2B_USB_Drive\Make_E2B_USB_Drive.cmd
if "%PP%"=="" if exist "%~dp0Make_E2B_USB_Drive\Make_E2B_USB_Drive.cmd" set PP=%~dp0Make_E2B_USB_Drive\Make_E2B_USB_Drive.cmd
if not "%PP%"=="" "%PP%"
